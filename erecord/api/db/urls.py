# -*- coding: UTF-8 -*-
"""erecord.api.db.urls

Request Dispatcher

"""

from erecord.api.cmn.views_mixins import VpzIdentViewMixin
from erecord.api.cmn.views_mixins import FormatViewMixin
from erecord.api.db.models_mixins import VleMixin

class RequestGetPkgDataList(VpzIdentViewMixin, FormatViewMixin):

    def __init__(self, config, data):
        try:
            self.config = config
            FormatViewMixin.__init__(self, data)
            pkgname = self.get_pkgname_value(data=data)
            if pkgname is None:
                msg = "[RequestGetPkgDataList][__init__]"
                msg += " unavailable 'pkgname' value " + str(pkgname)
                raise Exception(msg)
            self.pkgname = pkgname
        except:
            raise

    def response(self):

        try :
            data_list = VleMixin.get_pkg_datafilename_list(config=self.config,
                                                          pkgname=self.pkgname)
            context = dict()
            context['datalist'] = data_list
            context['pkgname'] = self.pkgname
            response = self.get_renderer().render(context)
        except:
            raise

        return response


class RequestGetPkgList(FormatViewMixin):

    def __init__(self, config, data):
        self.config = config
        FormatViewMixin.__init__(self, data)

    def response(self):

        try :
            pkg_list = VleMixin.get_pkg_list(config=self.config)
            context = dict()
            context['pkglist'] = pkg_list
            response = self.get_renderer().render(context)
        except:
            raise

        return response


class RequestGetPkgVpzList(VpzIdentViewMixin, FormatViewMixin):

    def __init__(self, config, data):

        try:
            self.config = config
            FormatViewMixin.__init__(self, data)
            pkgname = self.get_pkgname_value(data=data)
            if pkgname is None:
                msg = "[RequestGetPkgVpzList][__init__]"
                msg += " unavailable 'pkgname' value " + str(pkgname)
                raise Exception(msg)
            self.pkgname = pkgname
        except:
            raise

    def response(self):

        try :
            vpz_list = VleMixin.get_pkg_vpz_list(config=self.config,
                                                 pkgname=self.pkgname)
            context = dict()
            context['vpzlist'] = vpz_list
            context['pkgname'] = self.pkgname
            response = self.get_renderer().render(context)
        except:
            raise

        return response

