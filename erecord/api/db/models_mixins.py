# -*- coding: utf-8 -*-
"""erecord.api.db.models_mixins

Mixins for models of the db application

"""

from django import forms

import os
from erecord.api.cmn.utils.vle import get_rep_pkgname_list
from erecord.api.cmn.utils.vle import get_rep_pkg_exp_path
from erecord.api.cmn.utils.vle import get_rep_pkg_data_path

class VleMixin(object):
    """additional methods for Vle objects : VlePkg ..."""

    @classmethod
    def get_pkg_datafilename_list(cls, config, pkgname):
        """Defines and returns the name list of data folder files of VlePkg
    
        The data folder is the default one : 'data' subdirectory of the
        vle package.
    
        'CMakeLists.txt' is excluded.
        """

        data_path = get_rep_pkg_data_path(rep_path=config.VLE_HOME,
                                          vle_version=config.VLE_VERSION,
                                          pkgname=pkgname)
        tmp = forms.FilePathField(path=data_path, recursive=True,
                allow_files=True, allow_folders=False)
        data_list = [ os.path.relpath(v, data_path) for (v,c) in tmp.choices ]
        n = "CMakeLists.txt"
        if n in data_list:
            data_list.remove(n)
        return data_list


    @classmethod
    def get_pkg_list(cls, config):
        """returns the list of the vle package names. """

        pkg_list = list()
        try :
            pkg_list = get_rep_pkgname_list(rep_path=config.VLE_HOME,
                                            vle_version=config.VLE_VERSION)
        except :
            pass
        return pkg_list


    @classmethod
    def get_pkg_vpz_list(cls, config, pkgname):
        """returns the list of vpz file names of pkg pkgname
    
        The vpz files taken into account are those found under one of the
        database vle packages (under its models repository path).
        """

        vpz_list = list()
        try :
            vlepkg_exp_path = get_rep_pkg_exp_path(rep_path=config.VLE_HOME,
                                             vle_version=config.VLE_VERSION,
                                             pkgname=pkgname)
            tmp = forms.FilePathField( path=vlepkg_exp_path, recursive=True,
                    allow_files=True, allow_folders=False, match=".*\.vpz$")
            for (v,c) in tmp.choices :
                vlevpzname = os.path.relpath( v, vlepkg_exp_path )
                vpz_list.append(vlevpzname)
        except :
            pass
        return vpz_list

