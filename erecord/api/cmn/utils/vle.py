# -*- coding: utf-8 -*-
"""api.cmn.utils.vle

Vle interface methods

Uses python os, os.path libraries.

Does not use pyvle.

"""

import os

def get_vle_bin_path(usr_path):
    return os.path.join(usr_path, 'bin')

def get_vle_lib_path(usr_path):
    return os.path.join(usr_path, 'lib')

def get_vle_pkgconfig_path(usr_path):
    return os.path.join(get_vle_lib_path(usr_path), 'pkgconfig')

def is_structured_as_vle_install(usr_path) :
    """determines if the usr_path is structured as a vle install directory.
    
    Returns True if the usr_path directory contains directories : bin, lib and
    lib/pkgconfig.
    """

    res = False
    bin_path = get_vle_bin_path(usr_path)
    lib_path = get_vle_lib_path(usr_path)
    pkgconfig_path = get_vle_pkgconfig_path(usr_path)
    if os.path.exists(bin_path) and os.path.isdir(bin_path) :
        if os.path.exists(lib_path) and os.path.isdir(lib_path) :
            if os.path.exists(pkgconfig_path) and os.path.isdir(pkgconfig_path) :
                res = True
    return res

def vle_version_type(version_name):
    """extracts and returns V from version name such as vle-V.rev"""

    v = version_name.split("-")[1]
    version_type = v.split(".")[0]
    return version_type

def is_structured_as_vle_version_name(version_name):
    """returns True if version_name is structured as vle-V.rev and else False

       (for example vle-1.1.3, vle-2.0.0)
    """

    version_name_structure_ok = False # default
    a = version_name.split("-") # ["vle", V.rev]
    if len(a) > 1 :
        if a[0] == "vle" :
            b = a[1]
            c = b.split(".") # [V, rev..]
            if len(c) > 1 :
                version_name_structure_ok = True
    return version_name_structure_ok

def get_pkgs_suffix(version_name):
    """returns suffix of pkgs name according to version_name """

    suffix = ""
    if is_structured_as_vle_version_name(version_name) :
        a = version_name.split("-") # ["vle", V.rev]
        b = a[1]
        c = b.split(".") # [V, rev..]
        suffix = c[0] + '.' + c[1]
    return suffix

def is_structured_as_vle_home(path, vle_version=None) :
    """determines if the path directory is structured as a VLE_HOME directory.
    
    Returns True if the path directory contains a pkgs subdirectory.
    """

    if vle_version is not None :
        pkgs_path = get_rep_pkgs_path(path, vle_version)
        if os.path.exists(pkgs_path) and os.path.isdir(pkgs_path) :
            return True
        return False

    else :
        elements = os.listdir(path)
        for e in elements :
            if e.startswith('pkgs-') :
                if os.path.isdir(os.path.join(path, e)) :
                    return True
        return False
            
def is_pkg_of_rep(rep_path, vle_version, pkgname) :
    """determines if a vle package exists (as a vle package) under a models
    repository path.

    The vle package name is pkgname.
    The models repository path is rep_path.

    Suppose that rep_path is a models repository path.
    Suppose that pkgname is a vle package.
    """
    if pkgname in get_rep_pkgname_list(rep_path, vle_version) :
        return True
    return False

def is_vpz_of_pkg_of_rep(rep_path, vle_version, pkgname, vpzname) :
    """determines if a vpz file exists as a vpz file of vle package under a
    models repository path.

    The vpz file name is vpzname.
    The vle package name is pkgname.
    The models repository path is rep_path.

    Suppose that rep_path is a models repository path.
    Suppose that pkgname is a vle package of the models repository.
    """

    res = False
    vpz_path = os.path.join(get_rep_pkg_exp_path(rep_path=rep_path,
        vle_version=vle_version, pkgname=pkgname), vpzname)
    if os.path.exists(vpz_path) and os.path.isfile(vpz_path) :
        res = True
    return res

def get_pkgs_name(vle_version) :
    return 'pkgs-' + get_pkgs_suffix(vle_version)

def get_exp_name() :
    return 'exp'

def get_data_name() :
    return 'data'

def get_output_name() :
    return 'output'

def get_rep_pkgs_path(rep_path, vle_version) :
    """returns the 'pkgs' directory path of a models repository
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    """

    return os.path.join(rep_path, get_pkgs_name(vle_version))

def get_rep_pkgname_list(rep_path, vle_version) :
    """returns the list of the vle packages names of a models repository.

    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    
    The vle packages taken into account are those found (as vle packages)
    under rep_path.
    """

    pkgname_list = list()
    rep_pkgs_path = get_rep_pkgs_path(rep_path, vle_version)

    #if os.path.exists(rep_pkgs_path) and os.path.isdir(rep_pkgs_path) :
    #    pkgname_list = os.listdir(rep_pkgs_path)
    #for pkgname in pkgname_list :
    #    if not os.path.isdir(os.path.join(rep_pkgs_path, pkgname)) :
    #        pkgname_list.remove(pkgname)
    s = list()
    if os.path.exists(rep_pkgs_path) and os.path.isdir(rep_pkgs_path) :
        s = os.listdir(rep_pkgs_path)
    for pkgname in s :
        if os.path.isdir(os.path.join(rep_pkgs_path, pkgname)) :
            pkgname_list.append(pkgname)
    return pkgname_list

def get_rep_pkg_path(rep_path, vle_version, pkgname) :
    """returns the path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    """

    return os.path.join(get_rep_pkgs_path(rep_path, vle_version), pkgname)

def get_rep_pkg_exp_path(rep_path, vle_version, pkgname) :
    """returns the 'exp' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    The vpz files taken into account are those found (as vpz files of the
    pkgname vle package) under rep_path.
    """

    return os.path.join(get_rep_pkg_path(rep_path, vle_version, pkgname),
                        get_exp_name())

def get_rep_pkg_data_path(rep_path, vle_version, pkgname) :
    """returns the 'data' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    """

    return os.path.join(get_rep_pkg_path(rep_path, vle_version, pkgname),
                        get_data_name())

def get_rep_pkg_output_path(rep_path, vle_version, pkgname) :
    """returns the 'output' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    """

    return os.path.join(get_rep_pkg_path(rep_path, vle_version, pkgname),
                        get_output_name())

def get_pkg_output_path(pkg_path) :
    """returns the 'output' directory path of a vle package.
    
    The vle package is located at pkg_path.
    """

    return os.path.join(pkg_path, get_output_name())

