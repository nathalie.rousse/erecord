# -*- coding: utf-8 -*-
"""api.cmn.utils.coding

Coding methods

"""

import json

unicode = str #WS python3

def byteify(input):
    """unicode to utf-8

    Convert any decoded JSON object from using unicode strings to
    UTF-8-encoded byte strings

    """
    
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.items()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def PREC_get_val(value):
    """JSON object to utf-8 """

    print("[get_val]")
    print("- value:", value, type(value))
    print("- json.loads(value):", json.loads(value), type(json.loads(value)))
    return byteify(json.loads(value))

