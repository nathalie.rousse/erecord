# -*- coding: UTF-8 -*-
"""erecord.api.cmn.utils.logger """

import logging
import os

def get_logger(config, name):
    """Creates a logger

    Creates a logger with LOGGING_FILE file destination (if not None) and
    with console destination (if IN_PRODUCTION False).
    """

    try:
        IN_PRODUCTION = config.IN_PRODUCTION
        LOGGING_FILE = config.LOGGING_FILE
    except Exception:
        pass
    if (IN_PRODUCTION is not True) and (IN_PRODUCTION is not False) :
        IN_PRODUCTION = False
    if LOGGING_FILE is not None :
        if not os.path.isdir(os.path.dirname(LOGGING_FILE)):
            LOGGING_FILE = None 

    # create logger
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s , %(name)s , %(levelname)s >> %(message)s')

    if not IN_PRODUCTION :

        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        logger.addHandler(ch)

    if LOGGING_FILE is not None :

        # create file handler 
        fh = logging.FileHandler(filename=LOGGING_FILE)
        if not IN_PRODUCTION :
            level = logging.DEBUG
        else : # default, IN_PRODUCTION
            level = logging.INFO # .WARNING ?
        fh.setLevel(level)

        # add formatter to fh
        fh.setFormatter(formatter)

        # add fh to logger
        logger.addHandler(fh)

    return logger

