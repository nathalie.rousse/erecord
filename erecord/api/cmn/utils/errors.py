# -*- coding: UTF-8 -*-
"""api.cmn.utils.errors

Utils to manage errors

"""

import traceback

def complete_error_message(errormsg, msg) :
    """Returns message error completed by msg if not already into """

    if msg in errormsg :
        return errormsg
    elif errormsg == "" :
        return (msg + errormsg)
    else :
        return (msg + " -- " + errormsg)

def get_exception_info(exception) :
    """Returns exception information"""

    errortype = type(exception).__name__
    errordetails = exception.args
    return (errortype, errordetails)

def build_error_content(exception, errormsg=""):

    errormsg = complete_error_message(errormsg, "[ERROR]")
    (errortype, errordetails) = get_exception_info(exception)
    more = ""
    for v in errordetails:
        more = complete_error_message(str(v), str(more))
    content = dict()
    content['error'] = errormsg
    content['more'] = more
    content['type'] = errortype
    return content

def logger_report_error(LOGGER) :
    """ Reports an error to log (better raise error !) """

    LOGGER.error(u"Error report :\n %s ", traceback.format_exc())

