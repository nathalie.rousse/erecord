"""erecord.api.cmn.views_mixins

Mixins common to erecord applications views

"""

from rest_framework.renderers import JSONRenderer
from rest_framework_yaml.renderers import YAMLRenderer
from rest_framework_xml.renderers import XMLRenderer

from erecord.api.cmn.serializers import getKeyOptionValues
from erecord.api.cmn.serializers import FormatOptionSerializer

# Some mixins common to erecord applications views

class FormatViewMixin(object):
    """additional methods for views having format option and about formats

    formats : .api, .json, .yaml, .xml
    """

    def __init__(self, data):
        self.format = self.get_format_value(data=data)

    def get_format_value(self, data):
        """returns the format available value found in data and else default

        Available values : 'json' 'yaml' 'xml' (no 'html', no 'api')

        Available values else than 'json' : enforced to 'json' default value
        """
        p = FormatOptionSerializer(data=data)
        p.is_valid()
        f = p.data['format']
        if (f != 'yaml') and (f != 'xml') :
            f = "json" 
        return f

    def get_renderer(self):

        # Available format values : 'json' 'yaml' 'xml'
        if self.format == 'yaml':
            return YAMLRenderer()
        elif self.format == 'xml':
            return XMLRenderer()
        else: # self.format == 'json':
            return JSONRenderer()

class DataViewMixin(object):
    """additional methods for views managing both GET and POST requests"""

    def request_data(self, request):
        return request.data

class ModeOptionViewMixin(object):
    """additional methods for views having mode option
    
    The 'mode' option is used for different choices. Some of them may not
    be available in some calling/view cases. Some may be incompatible each 
    other.

    The class has been completed in order to also accept some other intuitive
    option names : 'style', 'plan', 'restype'.

    (for more information, see doc folder)
    """

    @classmethod
    def get_mode_option_values(cls, data):
        """Option 'mode' (several values are possible)
    
        Returns the list of mode option values.

        The 'mode' option is used for different choices. Some of them may not
        be available in some calling/view cases. Some may be incompatible
        each other.

        The method has been completed in order to also accept, instead of
        'mode' name, some other intuitive option names : 'style', 'plan',
        'restype'.
        """

        available_values = [ 'tree', 'link', 'compact', 'compactlist', # style 
                'single', 'linear', # plan
                'dataframe', 'matrix', # restype
                'todownload', # todownload
                'storage', # storage
                'bycol', # bycol
                ]

        s = getKeyOptionValues(data=data, key='style')
        s_style = list()
        for v in ['tree', 'link', 'compact', 'compactlist'] :
            if v in s :
                s_style.append(v)

        s = getKeyOptionValues(data=data, key='plan')

        s_plan = list()
        for v in ['single', 'linear']:
            if v in s :
                s_plan.append(v)

        s = getKeyOptionValues(data=data, key='restype')
        s_restype = list()
        for v in ['dataframe', 'matrix']:
            if v in s :
                s_restype.append(v)

        s = getKeyOptionValues(data=data, key='mode') + s_style + s_plan + s_restype
        mode_option = list()
        if len(s) > 0:
            for v in available_values :
                if v in s :
                    mode_option.append(v)

        return mode_option

    def get_plan_value(self, data):
        """returns the plan value according to data

        plan = single, linear. Default value : single.
        """
        mode_options = self.get_mode_option_values(data=data)
        plan = 'single' # default
        if mode_options is not None :
            if 'linear' in mode_options :
                plan = 'linear'
            #elif 'single' in mode_options :
            #    plan = 'single'
        return plan

    def get_restype_value(self, data):
        """returns the restype value according to data

        restype = dataframe, matrix. Default value : dataframe.
        """
        mode_options = self.get_mode_option_values(data=data)
        restype = 'dataframe' # default
        if mode_options is not None :
            if 'matrix' in mode_options :
                restype = 'matrix'
            #elif 'dataframe' in mode_options :
            #    restype = 'dataframe'
        return restype

    def get_todownload_value(self, data):
        """returns the todownload value according to data

        todownload = True, False. Default value : False.
        """
        mode_options = self.get_mode_option_values(data=data)
        todownload = False # default
        if mode_options is not None :
            if 'todownload' in mode_options :
                todownload = True
        return todownload

    def get_storage_value(self, data):
        """returns the storage value according to data

        storage = True, False. Default value : False.
        """
        mode_options = self.get_mode_option_values(data=data)
        storage = False # default
        if mode_options is not None :
            if 'storage' in mode_options :
                storage = True
        return storage

    def get_bycol_value(self, data):
        """returns the bycol value according to data

        bycol = True, False. Default value : False.
        """
        mode_options = self.get_mode_option_values(data=data)
        bycol = False # default
        if mode_options is not None :
            if 'bycol' in mode_options :
                bycol = True
        return bycol

class DataFolderCopyViewMixin(object):
    """additional methods for views having datafoldercopy option """

    def get_datafoldercopy_value(self, data):
        """returns the datafoldercopy value according to data

        datafoldercopy = replace, overwrite. Default value : replace.
        """

        s = getKeyOptionValues(data=data, key='datafoldercopy')
        datafoldercopy = 'replace' # default
        if 'overwrite' in s and 'replace' not in s :
            datafoldercopy = 'overwrite'
        return datafoldercopy

class StyleViewMixin(ModeOptionViewMixin):
    """additional methods for views having style (in mode option)"""

    def get_style_value(self, data):
        """returns the style value according to data

        style = tree, compact, compactlist. Default value : compact.
        """

        mode_options = self.get_mode_option_values(data=data)
        style = 'compact' # default
        if mode_options is not None :
            if 'compact' not in mode_options :
                if 'compactlist' in mode_options :
                    style = 'compactlist'
                if 'tree' in mode_options :
                    style = 'tree'
        return style

class VpzIdentViewMixin(object):
    """additional methods for views having options to identify a vpz file

    Options : vpzname and pkgname, or vpz (vpz id number)
    """

    def get_vpzname_value(self, data):
        """returns the vpzname value according to data """

        key='vpzname'
        vpzname = None
        if key in data.keys():
            vpzname = data[key]
        return vpzname

    def get_pkgname_value(self, data):
        """returns the pkgname value according to data """

        key='pkgname'
        pkgname = None
        if key in data.keys():
            pkgname = data[key]
        return pkgname

    def get_vpz_value(self, data):
        """returns the vpz value according to data (vpz id number)

        Note : value enforced to int so that OK for vpz id given as string
        """

        key = 'vpz'
        vpz = None # default
        if key in data.keys():
            vpz = int(data[key])
        return vpz

class ActionViewMixin(object):
    """additional methods for action option

    action is the name of the method to be called/launched
    """

    @classmethod
    def get_action_list(cls):

        ACTION_LIST = [
                       # activity actions
                       'get_vpz_input', 'post_vpz_input',
                       'get_vpz_output', 'post_vpz_output',
                       'get_vpz_inout', 'post_vpz_inout',
                       'get_vpz_experiment', 'post_vpz_experiment',
                       'get_vpz_report', 'post_vpz_report',
                       'get_vpz_report_conditions',
                       'post_vpz_report_conditions',

                       'help', # default

                       'get_pkg_list',
                       'get_pkg_vpz_list',

                       'get_pkg_data_list'
                      ]
        return ACTION_LIST

    def is_activity_action(self):

        ACTIVITY_ACTION_LIST = [
                       'get_vpz_input', 'post_vpz_input',
                       'get_vpz_output', 'post_vpz_output',
                       'get_vpz_inout', 'post_vpz_inout',
                       'get_vpz_experiment', 'post_vpz_experiment',
                       'get_vpz_report', 'post_vpz_report',
                       'get_vpz_report_conditions',
                       'post_vpz_report_conditions'
                      ]
        return (self.action in ACTIVITY_ACTION_LIST)

    def is_help_action(self):
        return (self.action == 'help')

    def is_get_pkg_data_list_action(self):
        return (self.action == 'get_pkg_data_list')

    def is_get_pkg_list_action(self):
        return (self.action == 'get_pkg_list')

    def is_get_pkg_vpz_list_action(self):
        return (self.action == 'get_pkg_vpz_list')

    @classmethod
    def get_action_value(cls, data):
        """returns the action value according to data (with a default value)"""

        try:
            action = 'help' # default
            if 'action' in data.keys():
                a = data['action']
                if a in cls.get_action_list():
                    action = a
        except:
            raise

        return action

