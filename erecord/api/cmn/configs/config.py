# -*- coding: UTF-8 -*-
"""erecord.api.cmn.configs.config

Contains the configuration : paths, constants...

"""

import os
from erecord.api.cmn.utils.logger import get_logger


class Config():
    """Configuration able to be modified/adapted

    Adapt according to the container : vle, pkg, vpz

    Adapt according to the galaxy tool : jsonoutput_filename,
    experiment_filename, report_conditions_filename, report_filename
    """

    def __init__(self):
        """Init with default values"""

        #----------------------------------------------------------------------
        # memo tmp : info from container :
        #            VLE_HOME=/VLE/vle_home
        #            PKGS_FOLDER_NAME=pkgs-2.0

        # VLE default values
        VLE_HOME = "/VLE/vle_home"
        VLE_VERSION = "vle-2.0.0"
        VLE_USR_PATH = "/VLE/vle"

        self.VLE_HOME = VLE_HOME
        self.VLE_VERSION = VLE_VERSION
        self.VLE_USR_PATH = VLE_USR_PATH

        # Database
        # A vpz file is identified/selected by its 'vpzname','pkgname'.
        # Special case (erecord compatibility) :
        # DB allows to identify a vpz file by a vpz id
        # (that corresponds with the 'vpz' parameter of erecord requests)

        self.DB = dict()

        #self.DB = { 1312 : { "vpzname" : "wwdm.vpz", "pkgname" : "wwdm",
        #                     "vlepath" : VLE_HOME,
        #                     "vleversion" : VLE_VERSION,
        #                     "vleusrpath" : VLE_USR_PATH }
        #          }

        #----------------------------------------------------------------------
        # Folders and files

        # erecord project home path ; MUST EXIST !!! (see ...)
        self.PROJECT_HOME = os.path.join(os.getcwd(), "WS") # "./WS"

        # factory path
        self.factory_path = os.path.normpath(
                                    os.path.join(self.PROJECT_HOME, "factory"))

        # run workspace path
        self.RUN_HOME = os.path.join(self.factory_path, "run")

        # out path (folder where final restitution); MUST EXIST !!! (see ...)
        self.out_path = os.path.normpath(
                           os.path.join(self.PROJECT_HOME, "OUT")) # "./WS/OUT"

        # Restitution : json file name
        self.jsonoutput_filename = "output.json"            # tool.xml uses it

        # Restitution : reports file names

        # cases get_vpz_experiment, post_vpz_experiment
        self.experiment_filename = "experiment.xls"         # tool.xml uses it

        # cases get_vpz_report_conditions, post_vpz_report_conditions
        self.report_conditions_filename = "conditions.xls"  # tool.xml uses it
        
        # cases get_vpz_report, post_vpz_report
        self.report_filename = "report.zip"                 # tool.xml uses it

        #----------------------------------------------------------------------

        # IN_PRODUCTION value False or True
        # If IN_PRODUCTION, no console destination for logging information ...
        self.IN_PRODUCTION = False # True or False

        # log

        self.LOGGER = None # default

        self.LOG_FOLDERPATH = os.path.join(self.factory_path, 'log')
        self.LOG_FILENAME = 'erecord.log'

        # File destination of logging information
        # Value None for no file destination for logging information
        self.LOGGING_FILE = os.path.join(self.LOG_FOLDERPATH,
                                         self.LOG_FILENAME)

        # To (des)activate recording activities events into LOG_ACT_FILE
        self.LOG_ACT_ACTIVE = True

        # File where recorded activities events
        self.LOG_ACT_FILE = os.path.join(self.LOG_FOLDERPATH,
                                         'erecord_act.log' )

        #----------------------------------------------------------------------

    def set_LOGGER(self, name="erecord api"):
        self.LOGGER = get_logger(config=self, name=name)

    def set_VLE_HOME(self, vle_home):
        self.VLE_HOME = vle_home

    def set_VLE_VERSION(self, vle_version):
        self.VLE_VERSION = vle_version

    def set_VLE_USR_PATH(self, vle_usr_path):
        self.VLE_USR_PATH = vle_usr_path

    def DB_add_vpz(self, vpz_id, vpzname, pkgname):
        """Add a vpz into DB"""

        self.DB[vpz_id] = { "vpzname" : vpzname, "pkgname" : pkgname,
                             "vlepath" : self.VLE_HOME,
                             "vleversion" : self.VLE_VERSION,
                             "vleusrpath" : self.VLE_USR_PATH }

    def get_VLE_HOME(self):
        return self.VLE_HOME

    def get_VLE_VERSION(self):
        return self.VLE_VERSION

    def get_VLE_USR_PATH(self):
        return self.VLE_USR_PATH

    def get_DB(self):
        return self.DB

    def get_as_dict(self):

        d = {"VLE_HOME" : self.VLE_HOME,
             "VLE_VERSION" : self.VLE_VERSION,
             "VLE_USR_PATH" : self.VLE_USR_PATH,
             "DB" : self.DB,
             "PROJECT_HOME" : self.PROJECT_HOME,
             "factory_path" : self.factory_path,
             "RUN_HOME" : self.RUN_HOME,
             "out_path" : self.out_path,
             "jsonoutput_filename" : self.jsonoutput_filename,
             "experiment_filename" : self.experiment_filename,
             "report_conditions_filename" : self.report_conditions_filename,
             "report_filename" : self.report_filename,
             "IN_PRODUCTION" : self.IN_PRODUCTION,
             "LOG_FOLDERPATH" : self.LOG_FOLDERPATH,
             "LOG_FILENAME" : self.LOG_FILENAME,
             "LOGGING_FILE" : self.LOGGING_FILE,
             "LOG_ACT_ACTIVE" : self.LOG_ACT_ACTIVE,
             "LOG_ACT_FILE" : self.LOG_ACT_FILE
            }
        return d

