# -*- coding: UTF-8 -*-
"""erecord.api.cmn.urls

Request Dispatcher

"""

from erecord.api.cmn.views_mixins import ActionViewMixin
from erecord.api.cmn.views_mixins import FormatViewMixin

from erecord.api.vpz.urls import RequestActivity
from erecord.api.db.urls import RequestGetPkgDataList
from erecord.api.db.urls import RequestGetPkgList
from erecord.api.db.urls import RequestGetPkgVpzList

from erecord.api.cmn.utils.errors import build_error_content


class RequestAction(ActionViewMixin):
    """Main Dispatcher"""

    def __init__(self, config, data):
        """defines data and action, from data"""

        self.config = config
        self.data = data

        # self.action (name of the method to call)
        self.action = self.get_action_value(data=data)

    def response(self) :
        """launch the action and return its result"""

        try :

            if self.is_activity_action():

                request = RequestActivity(config=self.config,
                                          action=self.action, data=self.data)

                #print("--------------------------------")
                #print("[RequestAction]", "Request:")
                #print("- obj:", request.obj)
                #print("- action:", request.action)
                #print("- data:", request.data)

                response = request.response()

            elif self.is_help_action():

                request = RequestHelp(data=self.data)
                response = request.response()

            elif self.is_get_pkg_data_list_action():

                request = RequestGetPkgDataList(config=self.config,
                                                data=self.data)
                response = request.response()

            elif self.is_get_pkg_list_action():

                request = RequestGetPkgList(config=self.config,
                                            data=self.data)
                response = request.response()

            elif self.is_get_pkg_vpz_list_action():

                request = RequestGetPkgVpzList(config=self.config,
                                               data=self.data)
                response = request.response()

            else: # anything else
                msg = "[RequestAction][response]"+" "
                msg += "Unavailable 'action' value " + str(self.action)
                raise Exception(msg)

        except :
            raise

        return response


class RequestError(FormatViewMixin):

    def __init__(self, data):
        FormatViewMixin.__init__(self, data)

    def response(self, msg, e) :
        """builds error response and returns it"""

        content = build_error_content(exception=e, errormsg=msg)

        response = self.get_renderer().render(content)
        return response


class RequestHelp(FormatViewMixin):

    def __init__(self, data):
        FormatViewMixin.__init__(self, data)

    def response(self) :
        """builds help response and returns it"""

        #
        #
        #
        #  USE TEXT4HELP.txt
        #
        #

#get_help action=...
#
#+ get_action_list ?


        content = dict()
        content['help'] = "HELP text (TODO)"
        content['action_list'] = ['get_vpz_input', 'post_vpz_input',
                                  'get_vpz_output', 'post_vpz_output',
                                  'get_vpz_inout', 'post_vpz_inout',
                                  'get_vpz_experiment', 'post_vpz_experiment',
                                  'get_vpz_report', 'post_vpz_report',
                                  'get_vpz_report_conditions',
                                  'post_vpz_report_conditions',
                                  'help',
                                  'get_pkg_list', 'get_pkg_vpz_list',
                                  'get_pkg_data_list'
                                 ]
        #content['more'] = more

        response = self.get_renderer().render(content)
        return response

