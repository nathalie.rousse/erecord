# -*- coding: utf-8 -*-
"""api.cmn.serializers

Serializers common to erecord applications

"""

from rest_framework import serializers

# Some serializer common to erecord applications

def getKeyOptionValues(data, key):
    """returns list of values of key option

    data[key] = one value or a list of values or not exist
    """

    if key in data.keys():
        v = data[key]
        if not isinstance(v, list) :
            v = [ v ]
    else:
        v = []
    return v

class FormatOptionSerializer(serializers.Serializer):
    """Option 'format' """
    format = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'format' not in attrs.keys() :
            attrs['format']=None
        return attrs

class JwtOptionSerializer(serializers.Serializer):
    """Option 'jwt' for JWT (JSON Web Token) """
    jwt = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'jwt' not in attrs.keys() :
            attrs['jwt']=None
        return attrs

