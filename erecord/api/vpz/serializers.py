# -*- coding: UTF-8 -*-
"""erecord.api.vpz.serializers """

from rest_framework import serializers

class PREC_VpzSelectionOptionsSerializer(serializers.Serializer):
    """Id options from which to create a VpzAct
    
    The options information to select a VpzPath (option 'vpzpath') or a VleVpz 
    (option 'vpz'). One and only one option must remain (priority to 'vpzpath').
    """
    vpzpath = serializers.IntegerField( required=False )
    vpz = serializers.IntegerField( required=False )

    def validate(self, attrs):
        for k in ('vpzpath', 'vpz') :
            if k not in attrs.keys() :
                attrs[k]=None
        if attrs['vpzpath'] is not None :
            if attrs['vpz'] is not None :
                attrs['vpz']=None
        return attrs

class VpzActivityDetailSerializer(serializers.Serializer):
    """Some parameters/options to call a vpz activity (relative to a VpzAct)
    
    For more, see getVpzActivityDetailOptionValues.
    """

    format = serializers.CharField(required=False)
    pk = serializers.IntegerField(required=False)

    def validate(self, attrs):
        for k in ('format', 'pk'):
            if k not in attrs.keys() :
                attrs[k]=None
        return attrs

