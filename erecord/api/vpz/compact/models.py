# -*- coding: utf-8 -*-
"""erecord.api.vpz.compact.models

vpz model relative to compact style presentation

"""

class VleParCompact(object):
    """VlePar relative to compact style (minimum information around values)"""

    def __init__(self, pname, cname, value, selection_name):
        self.pname = pname
        self.cname = cname
        self.value = value
        self.selection_name = selection_name

class VleCondCompact(object):
    """VleCond relative to compact style"""

    def __init__(self, selection_name):
        self.selection_name = selection_name

class VleViewCompact(object):
    """VleView relative to compact style"""

    def __init__(self, selection_name):
        self.selection_name = selection_name

class VleOutCompact(object):
    """VleOut relative to compact style"""

    def __init__(self, selection_name):
        self.selection_name = selection_name

