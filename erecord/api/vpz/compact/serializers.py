# -*- coding: UTF-8 -*-
"""erecord.api.vpz.compact.serializers"""

from rest_framework import serializers

from erecord.api.vpz.compact.models import VleParCompact

#------------------------------------------------------------------------------

class VpzInputCompact(object):
    """VpzInput relative to compact style (minimum information around values)
    
    This presentation should help user preparing its request (see "pars",
    "outselect"...)

    Only selected VlePar (and their VleCond) of VpzInput are kept.
    Only selected VleOut (and their VleView) of VpzInput are kept.
    """

    def __init__(self, begin=None, duration=None, pars=None, conds=None, 
            views=None, outs=None):
        self.begin = begin
        self.duration = duration
        self.pars = list()
        if isinstance(pars, list) :
            for par in pars :
                self.pars.append(par)
        self.conds = list()
        if isinstance(conds, list) :
            for cond in conds :
                self.conds.append(cond)
        self.views = list()
        if isinstance(views, list) :
            for view in views :
                self.views.append(view)
        self.outs = list()
        if isinstance(outs, list) :
            for out in outs :
                self.outs.append(out)

class VleParCompactSerializer(serializers.Serializer):
    pname = serializers.CharField(max_length=200)
    cname = serializers.CharField(max_length=200)
    selection_name = serializers.CharField(max_length=200)
    #WS value not controlled (not CharField !)

class VleCondCompactSerializer(serializers.Serializer):
    selection_name = serializers.CharField(max_length=200)

class VleViewCompactSerializer(serializers.Serializer):
    selection_name = serializers.CharField(max_length=200)

class VleOutCompactSerializer(serializers.Serializer):
    selection_name = serializers.CharField(max_length=200)

class VpzInputCompactSerializer(serializers.Serializer):
    """Some options to modify VpzInput """

    begin = serializers.FloatField(required=False)
    duration = serializers.FloatField(required=False)
    pars = VleParCompactSerializer(many=True, required=False, read_only=False)
    conds = VleCondCompactSerializer(many=True, required=False, read_only=False)
    views = VleViewCompactSerializer(many=True, required=False, read_only=False)
    outs = VleOutCompactSerializer(many=True, required=False, read_only=False)

    def update(self, instance, validated_data):
        """
        Given a dictionary of deserialized field values, update an existing
        VpzInputCompact instance.
        """
        #WS to update instance.pars, call update_pars !

        if 'begin' in validated_data.keys() :
            instance.begin = validated_data['begin']
        if 'duration' in validated_data.keys() :
            instance.duration = validated_data['duration']
        #if 'pars' in validated_data.keys() : #WS deleted => call update_pars!
        return instance

    def update_pars(self, instance, data): #WS added

        if 'pars' in data.keys() :
            pars = data['pars']
            for par in pars :
                if 'value' in par.keys():
                    value=par['value']
                    s = VleParCompactSerializer(data=par)
                    if s.is_valid() :
                        validated_par = s.data
                        instance.pars.append(VleParCompact(
                              cname=validated_par['cname'],
                              pname=validated_par['pname'],
                              value=value,
                              selection_name=validated_par['selection_name']))
        return instance

#------------------------------------------------------------------------------

