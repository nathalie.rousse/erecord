# -*- coding: UTF-8 -*-
"""erecord.api.vpz.urls

Request Dispatcher (activities)

"""

from erecord.api.vpz.views.activities import InputView
from erecord.api.vpz.views.activities import OutputView
from erecord.api.vpz.views.activities import InOutView
from erecord.api.vpz.views.activities import ReportView
from erecord.api.vpz.views.activities import ReportConditionsView
from erecord.api.vpz.views.activities import ExperimentView
from erecord.api.cmn.views_mixins import FormatViewMixin
from erecord.api.cmn.views_mixins import VpzIdentViewMixin
from erecord.api.cmn.views_mixins import ActionViewMixin

# WS ##########################################################################
#
# CAS non gardes :
# - format=html  => mode=storage non garde non plus
# - format=api
# - style=link
# - todownload ne sert plus
#
# UNIQUE format garde : format=json
#
# CAS gardes (absolument) :
# - format=json
# - style=compactlist (et compact) pour vpz/input
# - style=tree pour vpz/output
#
###############################################################################

# INPUT data dict
#
#------------------------------------------------------------------------------
#
# - key "action", values :
#   'get_vpz_input', 'post_vpz_input'
#   'get_vpz_output', 'post_vpz_output'
#   'get_vpz_inout', 'post_vpz_inout'
#   'get_vpz_experiment', 'post_vpz_experiment'
#   'get_vpz_report', 'post_vpz_report'
#   'get_vpz_report_conditions', 'post_vpz_report_conditions'
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_input, post_vpz_input :
#
# - keys :
#
#   - begin, duration
#   - pars or cname.pname
#   - parselect
#   - outselect
#   - style, mode for style
#     values : 'tree' 'compact' 'compactlist' (no 'link')
#   - format
#     values : 'json' 'yaml' 'xml'            (enforced to 'json')
#
# Note :
# - parselect : get_vpz_output, post_vpz_output
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_output, post_vpz_output :
#
# - keys :
#
#   - begin, duration
#   - pars or cname.pname
#   - outselect
#   - plan, restype, mode for plan and restype
#   - datafolder, datafoldercopy
#   - style, mode for style
#     values : 'tree' 'compact' ('compactlist' idem 'compact') (no 'link')
#   - format
#     values : 'json' 'yaml' 'xml'            (enforced to 'json')
#
# Note :
# - plan, restype, mode for plan and restype :
#   not for get_vpz_input, post_vpz_input
# - datafolder, datafoldercopy :
#   not for get_vpz_input, post_vpz_input
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_inout, post_vpz_inout :
#
#
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_experiment, post_vpz_experiment :
#
# keys in get_vpz_experiment case :
# - format
#
# keys in post_vpz_experiment case :
# - format
# - experimentfile
# - datafolder, datafoldercopy
#
# Note :
# - NON : mode for todownload
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_report, post_vpz_report :
#
# keys :
#
# - format
# - plan restype
# - begin duration pars or cname.pname
# - datafolder datafoldercopy
# - parselect outselect
# - report 
#   type : list or str
#   values : 'vpz' 'csv' 'txt' 'xls' 'all'
# - mode (for bycol)
#   type (of mode) : list or str
#   values : 'bycol' 'byrow' ...etc...
#
# Note :
# - NON : mode for todownload
#
#------------------------------------------------------------------------------
# Case of 'action' == get_vpz_report_conditions, post_vpz_report_conditions :
# 
# keys :
#
# - format
# - begin duration pars or cname.pname
# - parselect
#(- outselect ???)
# - mode (for bycol)
#   type (of mode) : list or str
#   values : 'bycol' 'byrow' ...etc...
#
# Note :
# - NON : mode for todownload
#
#------------------------------------------------------------------------------

class RequestActivity(VpzIdentViewMixin, ActionViewMixin, FormatViewMixin):

    def __init__(self, config, data, action=None):
        """defines data obj and action, from data

        Inputs :
        
         - data dict : see above
         - config : configuration that will have to be passed when required
        
        Outputs :
        
         - json data
        
         - 1 file in following cases : 
           - experiment.xls file
             if 'action' == get_vpz_experiment, post_vpz_experiment
           - conditions.xls file
             if 'action' == get_vpz_report_conditions,
                            post_vpz_report_conditions
           - report.zip file
             if 'action' == get_vpz_report, post_vpz_report
        
        Attributes :

         - self.data
         - self.obj    (dict keys : "vpzname" "pkgname",
                                    "vlepath", "vleversion", "vleusrpath")
         - self.action (name of the method to call)
         - self.config :
        """

        try:
            self.config = config # get configuration

            # self.format
            FormatViewMixin.__init__(self, data)

            self.data = data

            if action is None :
                action = self.get_action_value(data=data)
            self.action = action
            if not self.is_activity_action():
                msg = "[RequestActivity][__init__]"
                msg += " unavailable 'action' value " + str(self.action)
                raise Exception(msg)

            vpz = self.get_vpz_value(data=data)
            if vpz is not None:
                if vpz not in self.config.DB.keys():
                    msg = "[RequestActivity][__init__]"
                    msg += " unavailable 'vpz' value " + str(vpz)
                    raise Exception(msg)
                obj = self.config.DB[vpz]
                self.obj = obj
            else:
                obj = { "vlepath" : self.config.VLE_HOME,
                        "vleversion" : self.config.VLE_VERSION,
                        "vleusrpath" : self.config.VLE_USR_PATH }

                vpzname = self.get_vpzname_value(data=data)
                if vpzname is None:
                    msg = "[RequestActivity][__init__]"
                    msg += " unavailable 'vpzname' value " + str(vpzname)
                    raise Exception(msg)
                obj["vpzname"] = vpzname

                pkgname = self.get_pkgname_value(data=data)
                if pkgname is None:
                    msg = "[RequestActivity][__init__]"
                    msg += " unavailable 'pkgname' value " + str(pkgname)
                    raise Exception(msg)
                obj["pkgname"] = pkgname

                self.obj = obj
        except:
            raise

    def response(self):
        """launches the action and returns the response"""

        try:
            action = self.action
            if action == 'get_vpz_input':
                content = InputView(self.config).get(self)
            elif action == 'post_vpz_input':
                content = InputView(self.config).post(self)
            elif action == 'get_vpz_output':
                content = OutputView(self.config).get(self)
            elif action == 'post_vpz_output':
                content = OutputView(self.config).post(self)
            elif action == 'get_vpz_inout':
                content = InOutView(self.config).get(self)
            elif action == 'post_vpz_inout':
                content = InOutView(self.config).post(self)
            elif action == 'get_vpz_experiment':
                content = ExperimentView(self.config).get(self)
            elif action == 'post_vpz_experiment':
                content = ExperimentView(self.config).post(self)
            elif action == 'get_vpz_report':
                content = ReportView(self.config).get(self)
            elif action == 'post_vpz_report':
                content = ReportView(self.config).post(self)
            elif action == 'get_vpz_report_conditions':
                content = ReportConditionsView(self.config).get(self)
            elif action == 'post_vpz_report_conditions':
                content = ReportConditionsView(self.config).post(self)
            else:
                msg = "[RequestActivity][response]"+" "
                msg += "Unavailable 'action' value " + str(action)
                raise Exception(msg)
        except:
            raise

        response = self.get_renderer().render(content)
        return response

