# -*- coding: utf-8 -*-
"""erecord.api.vpz.models

Models of the vpz application

"""

import os
import json

from django.core.exceptions import ValidationError

from erecord.api.vpz.models_mixins.models_mixins import VpzActMixin
from erecord.api.vpz.models_mixins.models_mixins import VpzWorkspaceMixin
from erecord.api.vpz.models_mixins.models_mixins import VpzInputMixin
from erecord.api.vpz.models_mixins.models_mixins import VpzOutputMixin
from erecord.api.vpz.models_mixins.transform import VpzInputTransformMixin
from erecord.api.vpz.models_mixins.transform import VpzOutputTransformMixin
from erecord.api.vpz.models_mixins.transform import VpzActTransformMixin

from erecord.api.cmn.utils.vle import is_structured_as_vle_home
from erecord.api.cmn.utils.vle import is_pkg_of_rep
from erecord.api.cmn.utils.vle import is_vpz_of_pkg_of_rep
from erecord.api.cmn.utils.vle import is_structured_as_vle_version_name
from erecord.api.cmn.utils.vle import is_structured_as_vle_install

from erecord.api.cmn.utils.coding import byteify

from erecord.api.cmn.utils.erecord_package import vle_read_vpz
from erecord.api.cmn.utils.erecord_package import get_value_type
#from erecord.api.cmn.utils.erecord_package import print_vle_read_vpz_result

# vpzact :
# - vpzname = name of the vpz file. Path relative to 'exp' directory,
#   with .vpz extension (same as in vle).
# - pkgname = name of the vle package including the vpz file 'name'. 
#   The vpz file must be physically installed under the vle package
#   (under 'path' directory).
# - vlepath = directory path where the vle package 'pkg' including
#   the vpz file 'name' has been physically installed (directory
#   structured as a VLE_HOME directory)
# - vleversion = vle version name
# - vleusrpath = directory path where the vle version has been physically
#   installed.

class VpzAct(VpzActTransformMixin, VpzActMixin, object):
    """Vpz activity

    An activity about/on a vpz file is one of the following ones : 

    - vpz/input GET   : see (unmodified) input,
    - vpz/input POST  : modify input and see modified input,
    - vpz/output GET  : run (unmodified) input and see output,
    - vpz/output POST : modify input and run modified input and see output,
    - vpz GET  : idem vpz/output GET + see (unmodified) input,
    - vpz POST : idem vpz/output POTS + see modified input,

    Attributes :

    - vpzname : vpz file name
    - pkgname : vle package name
    - vlepath : vle home path (models repository path)

    - vleversion : vle version name
    - vleusrpath : path of the vle version install directory

    - vpzinput : relative VpzInput (one VpzAct to one VpzInput relationship) 
    - vpzoutput : relative VpzOutput (one VpzAct to one VpzOutput relationship)

    - created_at, updated_at : date information for admin management

    """

    def __init__(self):

        self.vpzname = None    # default
        self.pkgname = None    # default
        self.vlepath = None    # default
        self.vleversion = None # default
        self.vleusrpath = None # default
        self.vpzworkspace = None # default

    @classmethod
    def create(cls, vpzname, pkgname, vlepath, vleversion, vleusrpath):

        vpzact = VpzAct()
        vpzact.vpzname = vpzname
        vpzact.pkgname = pkgname
        vpzact.vlepath = vlepath
        vpzact.vleversion = vleversion
        vpzact.vleusrpath = vleusrpath
        #vpzact.vpzworkspace
        return vpzact

    #def clean(self):
    def control(self):

        if not is_structured_as_vle_version_name(self.vleversion) :
            msg = "The vle version name ("+self.vleversion+") is not "
            msg = msg + "structured as vle-V.rev."
            raise ValidationError(msg)

        if not is_structured_as_vle_home(self.vlepath, self.vleversion) :
            msg = "The path ("+self.vlepath+") is not a directory structured "
            msg = msg + "as a VLE_HOME directory."
            raise ValidationError(msg)

        if not is_pkg_of_rep(rep_path=self.vlepath,
                             vle_version=self.vleversion,
                             pkgname=self.pkgname) :
            msg = "'"+self.pkgname+ "' doesn't exist (as a vle package) "
            msg = msg + "under the vlepath ("+self.vlepath+")"
            raise ValidationError(msg)

        if not is_vpz_of_pkg_of_rep(rep_path=self.vlepath,
                                    vle_version=self.vleversion,
                                    pkgname=self.pkgname,
                                    vpzname=self.vpzname) :
            msg = "'"+self.vpzname+"' doesn't exist as a vpz file of the "
            msg = msg + "'"+self.pkgname+"' vle package under the vlepath "
            msg = msg + "("+self.vlepath+")"
            raise ValidationError(msg)

        if not is_structured_as_vle_install(self.vleusrpath) :
            msg = "The path ("+self.vleusrpath+") is not a directory "
            msg = msg + "structured as a vle install directory (containing "
            msg = msg + "directories : bin, lib, lib/pkgconfig)."
            raise ValidationError(msg)

    def __str__(self):
        return "Vpz activity on vpz file '"+ \
                self.vpzname+"' (of '"+self.pkgname+"' of '"+ \
                self.vlepath+"' of '"+ self.vleversion+"')"

class VpzInput(VpzInputTransformMixin, VpzInputMixin, object):
    """Vpz input

    - vpzact : relative VpzAct
      (one VpzAct to one VpzInput relationship) 

    - vlebegin : relative VleBegin
      (one VpzInput to one VleBegin relationship)
    - vleduration : relative VleDuration
      (one VpzInput to one VleDuration relationship)
    - vlecond_list : list of relative VleCond
      (many VleCond to one VpzInput relationship)
    - vleview_list : list of relative VleView
      (many VleView to one VpzInput relationship)
    """

    def __init__(self, vpzact):

        self.vpzact = vpzact       # VpzAct
        self.vlebegin = None       # VleBegin
        self.vleduration = None    # VleDuration
        self.vlecond_list = list() # of VleCond
        self.vleview_list = list() # of VleView

    def read_and_add_content(self, jres=None) :
        """reads VpzInput content (initial state) and add it into \
           the database.

        Reads into the vpz file (cf VpzAct) the vle input \
        information and add the relevant objects (VleBegin, \
        VleDuration, VleCond..., VleView, \
        VleOut) into the database.

        The VpzInput is attached to a VpzAct.

        output_plugin_choice that is used for VleView always values 'storage'.

        Uses read_vpz simulator of erecord package
        """

        output_plugin_choice='storage'

        vlehome_path = self.vpzact.vpzworkspace.get_vlehome()
        runvle_path = self.vpzact.vpzworkspace.define_and_build_runhome_subdirectory(rootname="read_")
        vle_version=self.vpzact.vleversion
        vle_usr_path=self.vpzact.vleusrpath
        pkgname=self.vpzact.pkgname
        vpzname=self.vpzact.vpzname
        try:
            jres = vle_read_vpz(runvle_path=runvle_path,
                            vlehome_path=vlehome_path,
                            vle_version=vle_version, vle_usr_path=vle_usr_path,
                            pkgname=pkgname, vpzname=vpzname)
        except:
            raise

        #print "LU dans le fichier : ", print_vle_read_vpz_result(jres)

        keys = jres.keys()

        if "begin" in keys :
            begin = jres["begin"]
            if "value" in begin.keys() :
                self.vlebegin = VleBegin(value=begin["value"])

        if "duration" in keys :
            duration = jres["duration"]
            if "value" in duration.keys() :
                self.vleduration = VleDuration(value=duration["value"])

        if "cond_list" in keys :
            cond_list = jres["cond_list"]
            for cname in cond_list.keys() :
                vlecond = VleCond(name=cname)
                vlecond.read_and_add_content(jcondition=cond_list[cname])
                self.vlecond_list.append(vlecond)

        if "view_list" in keys :
            view_list = jres["view_list"]
            for vname in view_list.keys() :

                jview = view_list[vname]
                type = jview["type"] #.encode()?       #u'type': u'timed'
                timestep = jview["timestep"]           #u'timestep': 1.0
                output = jview["output"]
                output_name = output["name"]           #u'name': u'view'
                output_plugin = output["plugin"]       #u'plugin': u'file'
                #old output_format = output["format"]  #u'format': u'local'
                output_location = output["location"]   #u'location': u''

                vleview = VleView(name=vname, type=type, 
                                  timestep=timestep, output_name=output_name, 
                                  output_plugin=output_plugin,
                                  output_location=output_location)
                vleview.read_and_add_content(jview=jview)
                self.vleview_list.append(vleview)

        self.update_vleout_and_save(output_plugin_choice=output_plugin_choice)

    def update_vlepar_values_and_save(self, vleparcompact_list) :
        """Updates VlePar values according to vleparcompact_list

        (list of VleParCompact).
        """
        for par in vleparcompact_list :
            for c in self.vlecond_list :
                for p in c.vlepar_list :
                    if par.selection_name==p.build_selection_name():
                        p.value = par.value #WS OK ?

    def set_selected_all_vlepars(self):
        for c in self.vlecond_list :
            for p in c.vlepar_list :
                p.set_selected()

    def unset_selected_all_vlepars(self):
        for c in self.vlecond_list :
            for p in c.vlepar_list :
                p.unset_selected()

    def update_vlepar_selected_and_save(self, parselect=None) :
        """Updates the VlePar attached to VpzInput, according to parselect.

        (for more about parselect values, see ParselectOptionViewMixin)

        Case parselect = None, or
        Case of "all" among the parselect list (priority given to this case) : 
        selected for all the VlePar.

        Case of some condname and/or parname (parname=cname.pname) into the
        parselect list :
        For each VleCond (with name) :
        (a) if name in parselect, then selected for all its VlePar.
        (b) if at least one parname relative to VleCond (ie with cname=name) in
          parselect, then selected for all its VlePar corresponding to one of
          those parname and unselected for all its other VlePar.
        (c) if no (a) and no (b), then unselected for all its VlePar.

        If a VleCond and at the same time (at least) one of its VlePar was
        given in parselect, there is no guarantee about which of both rules
        would be applied.
        """

        if not parselect:
            self.set_selected_all_vlepars()

        else : # if parselect :
            if "all" in parselect :
                self.set_selected_all_vlepars()
            else :
                self.unset_selected_all_vlepars() # default

                selected_cond_list = list()
                for c in self.vlecond_list :
                    if c.build_selection_name() in parselect :
                        selected_cond_list.append(c)
                cond_with_one_par_selected_list = list() 
                for c in self.vlecond_list :
                    for p in c.vlepar_list :
                        if (p.build_selection_name() in parselect) and \
                        (c not in cond_with_one_par_selected_list) :
                            cond_with_one_par_selected_list.append(c)
                for c in self.vlecond_list :
                    if c in cond_with_one_par_selected_list :
                        for p in c.vlepar_list :
                            if p.build_selection_name() in parselect :
                                p.set_selected()
                            else :
                                p.unset_selected()
                    elif c in selected_cond_list :
                        for p in c.vlepar_list :
                            p.set_selected()
                    else :
                        for p in c.vlepar_list :
                            p.unset_selected()

                if not selected_cond_list and \
                   not cond_with_one_par_selected_list : # no (a) and no (b)
                    self.set_selected_all_vlepars() #WS ???

    def update_vleout_and_save(self, output_plugin_choice='storage') :
        """Updates into the database the VleOut that are attached
           to VpzInput, according to output_plugin_choice.

        (for more about output_plugin_choice, see output_plugin_choice, 
        get_kind_of_simulation_result in ReportOptionViewMixin)

        Initialisation of all VleOut selected according to 
        output_plugin_choice.

        A view activation operation depends on output_plugin_choice value.
        """

        # VleOut selected initialisation
        for v in self.vleview_list :
            if v.is_activated(output_plugin_choice) :
                for o in v.vleout_list :
                    o.set_selected()

    def get_vlecond_selected(self):
        """Filter consisting in keeping only the selected VleCond
        
        A VleCond is selected if at least one of its VlePar
        is selected
        """

        vlecond_selected_list = list()
        for c in self.vlecond_list:
            if c.get_vlepar_selected():
                vlecond_selected_list.append(c)
        return vlecond_selected_list

    def get_vleview_selected(self): #WS n'est plus valable ???
        """Filter consisting in keeping only the selected VleView
        
        A VleView is selected if at least one of its VleOut
        is selected
        """

        vleview_selected_list = list()
        for v in self.vleview_list:
            if v.get_vleout_selected():
                vleview_selected_list.append(v)
        return vleview_selected_list

    def is_without_any_view_in_storage_mode(self):
        """Returns if it has or not any VleView in storage mode
        """
        for v in self.vleview_list :
            if v.is_storage_mode() :
                return False
        return True

class VpzOutput(VpzOutputTransformMixin, VpzOutputMixin, object):
    """Vpz output

    - vpzact : relative VpzAct
      (one VpzAct to one VpzOutput relationship) 

    - res : simulation numerical result
    - plan : kind of plan of the simulation (single or linear)
    - restype : kind of restype of the simulation (dataframe or matrix)

    """

    def __init__(self, vpzact):

        self.vpzact = vpzact  # VpzAct
        self.res = None       # str
        self.plan = None      # str
        self.restype = None   # str

    def run_and_add_content(self, plan='single', restype='dataframe'):
        """Simulates, reads VpzOutput content (res).

        Simulates the vpz file relative to VpzAct. 
        Reads the vle output information and add the relevant objects 
        (VleView...) into the database. The VpzOutput is 
        attached to a VpzAct.

        The method does not use (is not based on) a VpzInput.
        
        The simulation running method depends on plan ('single' or 'linear')
        and restype ('dataframe' or 'matrix').
        """

        try:
            self.run(plan=plan, restype=restype)
            #not self.res = json.dumps(self.res) #WS (on reste python)
        except:
            raise

class VleBegin(object):
    """Vle begin

    Attributes :

    - value : value (in vle, begin value)
    - vpzinput : relative VpzInput (one VleBegin to one VpzInput relationship) 
    """

    def __init__(self, value):
        self.value = value # float

class VleDuration(object):
    """Vle duration

    Attributes :

    - value : value (in vle, duration value)
    - vpzinput : relative VpzInput (one VleDuration to one VpzInput 
      relationship) 
    """

    def __init__(self, value):
        self.value = value # float

# vlecond :
# - name = Condition name (same as in vle)
# - vpzinput = the vpz input to which the vle condition belongs

class VleCond(object):
    """Vle condition

    - name : name (in vle, condition name)
    - vlepar_list : list of relative VlePar (many VlePar to one VleCond
      relationship)
    - vpzinput : relative VpzInput (many VleCond to one VpzInput relationship) 
    """

    def __init__(self, name):
        self.name = name          # str
        self.vlepar_list = list() # of VlePar

    def get_vlepar_selected(self):
        """Filter consisting in keeping only the selected VlePar """

        vlepar_selected_list = list()
        for vlepar in self.vlepar_list:
            if vlepar.selected == True:
                vlepar_selected_list.append(vlepar)
        return vlepar_selected_list

    def read_and_add_content(self, jcondition=None) :
        """reads VleCond content (initial state) and add it into the
        database

        Reads into the vpz file (cf VpzAct) the vle condition
        information (initial state) and add the relevant objects
        (VlePar) into the database.
        The VleCond is attached to a VpzInput that is
        attached to a VpzAct.

        Uses json result based on read_vpz simulator of erecord package
        more exactly : jcondition is the condition (from json result)
        """

        try:
            if jcondition is not None :
                if "par_list" in jcondition.keys() :
                    par_list = jcondition["par_list"]
                    for pname in par_list.keys() :
                        parameter = par_list[pname]
                        parameter_type = get_value_type(parameter["type"])
                        parameter_value = parameter["value"]
                        vlepar = VlePar(pname=pname, cname=self.name, 
                                        type=parameter_type,
                                        value=parameter_value,
                                        # not json.dumps(parameter_value)
                                        selected=True)
                        self.vlepar_list.append(vlepar)
        except:
            raise

    def build_selection_name(self) :
        return (self.name)

# vlepar :
# - pname = Name of the vle parameter (same as in vle)
# - cname = Name of the vle parameter condition (same as in vle)
# - type = Type of the vle parameter (...of the first value)
# - value = Value of the vle parameter (...charfield for the moment)
# - selected = Selected or not for restitution
# - vlecond = the vle condition to which the vle parameter belongs

class VlePar(object):
    """Vle parameter (of a VleCond)

    Attributes :

    - pname : parameter name (in vle, port name)
    - cname : condition name (in vle, condition name)
    - type : value type (in vle, port value type)
    - value : value (in vle, port value)
    - selected : selection or not of the parameter for restitution
    - vlecond : relative VleCond (many VlePar to one VleCond relationship)

    """
    def __init__(self, pname, cname, type, value, selected=False):

        self.pname = pname        # str
        self.cname = cname        # str
        self.type = type          # str
        self.value = value        # str
        self.selected = selected  # boolean

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_parameter_selection_name(cls, cname, pname):
        """Defines and returns the selection name of a parameter.
        
        Attention : if the selection name did not value cname.pname anymore, 
        then get_vleparcompact_option_values method code
        (of erecord.api.vpz.compact.views_mixins.VleParCompactOptionViewMixin)
        should be modified/adapted !
        """
        return (cname+"."+pname)

    def build_selection_name(self) :
        """Defines and returns the selection name of a parameter.

        For consistency, must call build_parameter_selection_name (see 
        get_vleparcompact_option_values method
        (of erecord.api.vpz.compact.views_mixins.VleParCompactOptionViewMixin)
        """
        return self.build_parameter_selection_name(cname=self.cname,
                pname=self.pname)

    def PREC_get_val(self):
        return byteify(json.loads(self.value))

# vleview :
# - name = View name (same as in vle)
# - type = View type (same as in vle)
# - timestep = View timestep (same as in vle)
# - output_name = View output name (same as in vle)
# - output_plugin = View output plugin (same as in vle)
# - output_format = View output format (same as in vle)
# - output_location = View output location (same as in vle)
# - vpzinput = the vpz input to which the vle view belongs

class VleView(object):
    """Vle view

    - name : name (in vle, view name)
    - type : type (in vle, view type)
    - timestep : time step (in vle, view time step)
    - output_name : output name (in vle, view output name)
    - output_plugin : output plugin (in vle, view output plugin)
    - output_format : output format (in vle, view output format)
    - output_location : output location (in vle, view output location)
    - vleout_list : list of relative VleOut
      (many VleOut to one VleView relationship)
    - vpzinput : relative VpzInput (many VleView to one VpzInput relationship) 

    Only the Vle output datas (VleOut) that exist at the initial state will
    be taken into account (not the ones created during the simulation).
    """

    def __init__(self, name, type, timestep, output_name,
                 output_plugin, output_location, output_format="obsolete"):

        self.name = name                       # str
        self.type = type                       # str
        self.timestep = timestep               # timestep float from vle
        self.output_name = output_name         # str
        self.output_plugin = output_plugin     # str
        self.output_format = output_format
        self.output_location = output_location # str
        self.vleout_list = list() # of VleOut

    def get_vleout_selected(self):
        """Filter consisting in keeping only the selected VleOut """

        #vleout_selected_list = self.vleout_list.all().filter(selected=True)
        vleout_selected_list = list()
        for vleout in self.vleout_list:
            if vleout.selected == True:
                vleout_selected_list.append(vleout)
        return vleout_selected_list

    def read_and_add_content(self, jview=None) :
        """reads VleView content (initial state) and add it into the database

        Reads into the vpz file (cf VpzAct) the vle view information
        (initial state) and add the relevant objects (VleOut) into
        the database.
        The VleView is attached to a VpzInput that is
        attached to a VpzAct.

        Uses json result based on read_vpz simulator of erecord package
        more exactly : jview is the view (from json result)
        """

        outputdata_name_list = [] # default
        if "out_list" in jview.keys() :
            outputdata_name_list = jview["out_list"]
                #u'out_list': [u'top:wwdm.LAI', u'top:wwdm.ST', u'top:wwdm.U']
                #.encode()?
        for oname in outputdata_name_list :
            l = oname.split(".")
            if len(l) == 2 :
                modelname = l[0]
                shortname = l[1]
            nameinres = oname  # nameinres=nameinvle now
            vleout = VleOut(oname=nameinres, vname=self.name, 
                            shortname=shortname, selected=False)
            self.vleout_list.append(vleout)

    def is_storage_mode(self) :
        return (self.output_plugin == "storage")
    def is_file_mode(self) :
        return (self.output_plugin == "file")
    def is_activated(self, output_plugin_choice) :
        return (self.output_plugin == output_plugin_choice)
    def unactivate(self) :
        self.output_plugin = "dummy"
    def activate(self, output_plugin_choice):
        self.output_plugin = output_plugin_choice

    def build_selection_name(self) :
        return (self.name)

# vleout :
# - oname = Name of the vle output data (same as in vle)
# - vname = Name of the vle output data view (same as in vle)
# - shortname = Shortname of the vle output data
# - selected = Selected or not for retitution
# - vleview = the vle view to which the vle output data belongs

class VleOut(object):
    """Vle output data (of a VleView)

    Attributes :

    - oname : output data name (in vle, port name)
    - vname : view name (in vle, view name)
    - shortname : short name
    - selected : selection or not of the output data for restitution
    - vleview : relative VleView (many VleOut to one VleView relationship)

    """

    def __init__(self, oname, vname, shortname, selected=False):

        self.oname = oname           # str
        self.vname = vname           # str
        self.shortname = shortname   # str
        self.selected = selected     # boolean

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_output_selection_name(cls, vname, oname):
        """Defines and returns the selection name of an output data """
        return (vname+"."+oname)

    def build_selection_name(self) :
        """Defines and returns the selection name of an output data """
        return self.build_output_selection_name(vname=self.vname,
                oname=self.oname)

# vpzworkspace :
# - homepath = absolute path of the workspace directory

class VpzWorkspace(VpzWorkspaceMixin, object):
    """Vpz workspace

    A VpzWorkspace is attached to a VpzAct. It defines a workspace
    directory dedicated to the Vpz activity/manipulation.

    Attributes :

    - vpzact : relative VpzAct (one VpzAct to one VpzWorkspace relationship) 
    - homepath : absolute path
    - vlehome : VLE_HOME path (defined if needed/used)
    - reporthome : report home path (defined if needed/used)
    - datahome : (sent) data home path (defined if needed/used)
    - runhome : run home path (defined if needed/used)

    todo : see when to be deleted, homepath deletion
    """

    def __init__(self, config) :

        self.config = config
        self.homepath = None
        self.vlehome = None
        self.reporthome = None
        self.datahome = None
        self.runhome = None

    @classmethod
    def create(cls, config, as_vlehome=False, as_reporthome=False,
               as_datahome=False, as_runhome=False):
        """create a VpzWorkspace attached to vpzact """
        vpzworkspace = VpzWorkspace(config)
        vpzworkspace.define_and_build(as_vlehome=as_vlehome,
                       as_reporthome=as_reporthome, as_datahome=as_datahome,
                       as_runhome=as_runhome)
        return vpzworkspace

    def control(self):
        """verifies that path directories exist"""

        if not os.path.isdir(self.homepath) :
            msg = "unavailable homepath directory '"+self.homepath+"'"
            raise ValidationError(msg)
        #if not self.is_undefined_value(self.vlehome):
        #    if not os.path.isdir(self.vlehome) :
        #        msg = "unavailable vlehome directory '"+self.vlehome+"'"
        #        raise ValidationError(msg)
        #if not self.is_undefined_value(self.reporthome):
        #    if not os.path.isdir(self.reporthome) :
        #        msg = "unavailable reporthome directory '"+self.reporthome+"'"
        #        raise ValidationError(msg)
        #if not self.is_undefined_value(self.datahome):
        #    if not os.path.isdir(self.datahome) :
        #        msg = "unavailable datahome directory '"+self.datahome+"'"
        #        raise ValidationError(msg)
        #if not self.is_undefined_value(self.runhome):
        #    if not os.path.isdir(self.runhome) :
        #        msg = "unavailable runhome directory '"+self.runhome+"'"
        #        raise ValidationError(msg)

