# -*- coding: UTF-8 -*-
"""api.vpz.views.objects_mixins

Mixins common to vpz application (objects)

"""

class VpzInputDetailViewMixin(object):
    """additional methods for views about VpzInput"""

    @classmethod
    def context(cls, vpzinput, style):

        if style=='compact' :
            context = cls.compact_context(vpzinput=vpzinput)
        elif style=='compactlist' :
            context = cls.compactlist_context(vpzinput=vpzinput)
        else: # style == 'tree'
            context = cls.tree_context(vpzinput=vpzinput)
        return context

    @classmethod
    def compact_context(cls, vpzinput):
        """ builds and returns the context relative to the Vpzinput, \
            in case of 'compact' style.

        More precisely, format may value 'json', 'api', etc. \
        style values 'compact'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.

        Only selected VlePar and selected VleOut are kept.
        A VleCond without any selected VlePar is not kept.
        A VleView without any selected VleOut is kept (cf output datas
        dynamically).

        """

        data = dict()
        data['begin'] = vpzinput.vlebegin.value
        data['duration'] = vpzinput.vleduration.value

        pars = list()
        conds = list()
        outs = list()
        views = list()

        for c in vpzinput.vlecond_list :
            at_least_one_vlepar_selected = False
            for p in c.vlepar_list :
                if p.is_selected():
                    par = {'cname':p.cname, 'pname':p.pname, 'value':p.value,
                           'selection_name':p.build_selection_name()}
                    pars.append(par)
                    at_least_one_vlepar_selected = True
            if at_least_one_vlepar_selected:
                cond = {'selection_name':c.build_selection_name()}
                conds.append(cond)
        for v in vpzinput.vleview_list :
            at_least_one_vleout_selected = False
            for o in v.vleout_list :
                if o.is_selected():
                    out = {'selection_name':o.build_selection_name()}
                    outs.append(out)
                    at_least_one_vleout_selected = True
            #if at_least_one_vleout_selected:
            #    views.append(... 
            view = {'selection_name':v.build_selection_name()}
            views.append(view)

        data['pars'] = pars
        data['conds'] = conds
        data['outs'] = outs
        data['views'] = views
        return data

    @classmethod
    def compactlist_context(cls, vpzinput):
        """ builds and returns the context relative to the Vpzinput, \
            in case of 'compactlist' style.

        More precisely, format may value 'json', 'api', etc. \
        style values 'compactlist'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.
        """

        data = dict()
        data['begin'] = vpzinput.vlebegin.value
        data['duration'] = vpzinput.vleduration.value
        vlecondcompact_list = list()
        for c in vpzinput.vlecond_list :
            at_least_one_vlepar_selected = False
            for p in c.vlepar_list :
                if p.is_selected():
                    data[p.build_selection_name()] = p.value
                    at_least_one_vlepar_selected = True
            if at_least_one_vlepar_selected:
                vlecondcompact_list.append(c.build_selection_name())
        data['conds'] = vlecondcompact_list
        vleviewcompact_list = list()
        vleoutcompact_list = list()
        for v in vpzinput.vleview_list :
            at_least_one_vleout_selected = False
            for o in v.vleout_list :
                if o.is_selected():
                    vleoutcompact_list.append( o.build_selection_name() )
                    at_least_one_vleout_selected = True
            if at_least_one_vleout_selected:
                vleviewcompact_list.append(v.build_selection_name())
        data['views'] = vleviewcompact_list
        data['outs'] = vleoutcompact_list
        return data

    @classmethod
    def tree_context(cls, vpzinput):
        """ builds and returns the context relative to the Vpzinput, \
            in case of 'tree' style.

        More precisely, format may value 'json', 'api', etc. \
        style values 'tree'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept. 

        Among all the VleCond, only the selected ones are serialized
        (that is to say the ones having at least one selected VlePar)
        Among all the VleView, only the selected ones are serialized 
        (that is to say the ones having at least one selected VleOut)
        """

        data = dict()

        begin = {'value':vpzinput.vlebegin.value}
        data['vlebegin'] = begin
        duration = {'value':vpzinput.vleduration.value}
        data['vleduration'] = duration

        cond_list = list()
        vlecond_selected_list = vpzinput.get_vlecond_selected()
        for vlecond in vlecond_selected_list :
            cond = {'name':vlecond.name}
            par_list = list()
            vlepar_selected_list = vlecond.get_vlepar_selected()
            for vlepar in vlepar_selected_list :
                par = {'pname':vlepar.pname, 'cname':vlepar.cname,
                       'value': vlepar.value, 'type': vlepar.type,
                       'selected': vlepar.selected}
                par_list.append(par)
            cond['vlepar_list'] = par_list
            cond_list.append(cond)
        data['vlecond_list'] = cond_list

        view_list = list()
        vleview_selected_list = vpzinput.get_vleview_selected()
        for vleview in vleview_selected_list :
            view = {'name': vleview.name,
                    'type': vleview.type,
                    'timestep': vleview.timestep,
                    'output_name': vleview.output_name,
                    'output_plugin': vleview.output_plugin,
                    'output_format': vleview.output_format,
                    'output_location': vleview.output_location}

            out_list = list()
            vleout_selected_list = vleview.get_vleout_selected()
            for vleout in vleout_selected_list :
                out = {'shortname':vleout.shortname,
                       'oname':vleout.oname,
                       'vname':vleout.vname,
                       'selected':vleout.selected}
                out_list.append(out)
            view['vleout_list'] = out_list
            view_list.append(view)
        data['vleview_list'] = view_list

        return data

class VpzOutputDetailViewMixin(object):
    """additional methods for views about VpzOutput"""

    # old 
    # no ZIPRenderer anymore; get_renderers has been replaced by get_renderer
    #def get_renderers(self):
    #    from erecord_cmn.renderers import ZIPRenderer
    #    template_name='erecord_vpz/headedform_vpzoutput_detail.html'
    #    r = self.get_specific_renderers(template_name)
    #    r.append( ZIPRenderer() )
    #    return r

    @classmethod
    def context(self, vpzoutput, style):

        context = dict()
        if style=='compact' or style=='compactlist' :
            context['res'] = vpzoutput.make_res_compact()
            context['plan'] = vpzoutput.plan
            context['restype'] = vpzoutput.restype
        else: # style == 'tree'
            context['res'] = vpzoutput.make_res_tree()
            context['plan'] = vpzoutput.plan
            context['restype'] = vpzoutput.restype
        return context

class VpzInOutDetailViewMixin(object):

    @classmethod
    def context(self, vpzact, vpzinput, vpzoutput, style):

        context = dict()
        context['vpzname'] = vpzact.vpzname
        context['pkgname'] = vpzact.pkgname
        context['vleversion'] = vpzact.vleversion
        context['vpzinput'] = VpzInputDetailViewMixin.context(
                                                vpzinput=vpzinput, style=style)
        context['vpzoutput'] = VpzOutputDetailViewMixin.context(
                                              vpzoutput=vpzoutput, style=style)
        return context

