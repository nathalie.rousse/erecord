# -*- coding: UTF-8 -*-
"""erecord.api.vpz.views.views_mixins

Mixins common to vpz application views

"""

from django.core.exceptions import ValidationError

from erecord.api.cmn.views_mixins import ModeOptionViewMixin
from erecord.api.vpz.views.activities_mixins import DataFolderViewMixin

from erecord.api.cmn.serializers import getKeyOptionValues

import os

from erecord.api.cmn.utils.dir_and_file import make_zip_folder
from erecord.api.cmn.utils.dir_and_file import create_dirs_if_absent

from erecord.api.cmn.utils.errors import build_error_content

#WS from erecord.api.cmn.utils.coding import get_val

from erecord.api.cmn.utils.erecord_package import vle_modify_vpz

class ReportViewMixin(object):
    """additional methods for views producing and returning reports
    
    NB : uses self.config

    The 'report' option is used to choose which reports are built into the
    produced/returned folder.

    report : for (into the report folder)

      - value 'vpz' : the vpz file.
      - value 'csv' : csv file(s).
      - value 'xls' : a xls file (several worksheets).
      - value 'txt' : txt file(s).
      - value 'all' : all the available files above (txt, csv...).

    """

    def get_report_values(self, data):
        """Defines the list of report values deduced from 'report' option 
        (several values are possible).

        The resulting reports may content several values for the kinds of
        report to be built and returned into the folder.

        Priority to 'all' value.
        Default values ['vpz', 'xls',]
        """

        available_values = ['vpz', 'csv', 'txt', 'xls',] # + special case 'all'
        
        s = getKeyOptionValues(data=data, key='report')
        report_option = list()
        if len(s) > 0:
            report_option = s

        #WS inutile ?
        #report_option = list(set(report_option)) # unicity

        if 'all' in report_option :
            reports = [o for o in available_values] # all
        else :
            reports = list()
            if report_option :
                reports = [o for o in report_option if o in available_values]
            if not reports : # default
                reports = ['vpz', 'xls',]
        return reports

    def build_zip_folder(self, vpzact):
        """builds the zip file of the report folder, and returns its path. """

        zip_path = make_zip_folder(self.get_folder_path(vpzact=vpzact))
        return zip_path

    def get_folder_path(self, vpzact) :
        """returns the report folder path name"""
        name = '__'+str(vpzact.pkgname)+'_'+str(vpzact.vpzname)+'__input__'
        return vpzact.vpzworkspace.get_report_folder_path(name)

    def build_folder(self, vpzact, vpzinput, vpzoutput, reports, bycol) :
        """builds content of the report folder depending on reports

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)

        if 'vpz' in reports:
            path = os.path.join(folderpath, 'exp')
            create_dirs_if_absent([folderpath, path,])
            vpzact.make_folder_vpz(vpzinput=vpzinput, dirpath=path)

        if 'csv' in reports or 'xls' in reports or 'txt' in reports:

            vpzact.prepare_reports(LOGGER=self.config.LOGGER,
                       vpzinput=vpzinput, vpzoutput=vpzoutput,
                       with_general_output=True,
                       default_general_output=False,
                       with_out_ident_and_nicknames=True, with_res=True,
                       with_bycol=bycol)

            if 'csv' in reports:
                path = os.path.join(folderpath, 'output')
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_csv_ident(dirpath=folderpath)
                vpzact.make_folder_csv_cond(dirpath=folderpath, bycol=bycol)
                vpzact.make_folder_csv_output(vpzoutput=vpzoutput,
                                              dirpath=path)
            if 'xls' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_xls(vpzoutput=vpzoutput,
                                       dirpath=path, bycol=bycol)
            if 'txt' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_txt(vpzinput=vpzinput, vpzoutput=vpzoutput,
                                       dirpath=path)

    def build_conditions_folder(self, vpzact, vpzinput, bycol) :
        """builds content of the simulation conditions report folder

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(LOGGER=self.config.LOGGER,
                           vpzinput=vpzinput, vpzoutput=None,
                           with_general_output=False,
                           default_general_output=False,
                           with_out_ident_and_nicknames=False, with_res=False,
                           with_bycol=bycol)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        outputfile_name = self.config.report_conditions_filename
        vpzact.make_conditions_folder_xls(dirpath=path,
                                        filename=outputfile_name, bycol=bycol)
        return outputfile_name

class ExperimentViewMixin(DataFolderViewMixin, ReportViewMixin):
    """additional methods for views about experiment

    NB : uses self.config

    The experiment information is into a xls file.

    Note : xlsx format not managed.
    """

    def getExperimentValues(self, data, vpzact) :
        """experiment values (conditions)

        Previously (changed) :
          The experiment information comes from the xls experiment file that
          is sent in POST data as key 'experimentfile'. It is saved (into
          workspace) as svg_experiment_file file, that is then read to extract
          the returned information (see read_experiment_file_xls).

        Now :
          The experiment information comes from the xls experiment file
          that is given in POST request
          (file name in POST data as key 'experimentfile').
          The experiment xls file is then read
          to extract the returned information (see read_experiment_file_xls).

        File format expected : .xls or .csv (file extension : .dat)

        """

        e = (False, None, None, None, None, dict(), list(), list()) # default
        try :
            if 'experimentfile' in data.keys() :
                experimentfile_path = data['experimentfile']
                e = vpzact.read_experiment_file_xls(
                                                  filepath=experimentfile_path)
        except Exception as e :
            msg = "[ExperimentViewMixin][getExperimentValues]"
            msg += ", " + "experimentfile: " + experimentfile_path
            cnt = build_error_content(exception=e, errormsg=msg)
            raise Exception(str(cnt))
        return e

    def modify_experiment(self, vpzact, begin_value=None, duration_value=None,
                          parameters=None, outselect_values=None) :
        """Modifies the vpz file (relative to vpzact) according to inputs.

        parameters is a dict (selection_name as key, value as value).

        outselect_values : list of the values of the 'outselect' option that
        is used to select the restituted output datas. 

        Returns VpzAct if no error, and raises exception else.

        Uses modify_vpz simulator of erecord package.

        The vpz file (relative to vlehome of vpzact.vpzworkspace) is modified.
        """

        try :
            vlehome_path = vpzact.vpzworkspace.get_vlehome()
            runvle_path = vpzact.vpzworkspace.define_and_build_runhome_subdirectory(rootname="modify_")
            vle_version = vpzact.vleversion
            vle_usr_path = vpzact.vleusrpath
            pkgname = vpzact.pkgname
            vpzname = vpzact.vpzname

            # builds storaged_list, conditions, begin, duration from data

            storaged_list = outselect_values

            conditions = dict()
            if parameters is not None :
                vleparcompact_option = self.get_vleparcompact_option_values(
                                                               data=parameters)
                for par in vleparcompact_option :
                    cname = par.cname # byteify(par.cname)
                    pname = par.pname # byteify(par.pname)
                    value = par.value #WS remplace # value = get_val(par.value)
                    if cname not in conditions.keys():
                        conditions[cname] = dict()
                    conditions[cname][pname] = value

            begin = begin_value
            duration = duration_value

            vle_modify_vpz(runvle_path=runvle_path,
                           vlehome_path=vlehome_path,
                           vle_version=vle_version,
                           vle_usr_path=vle_usr_path,
                           pkgname=pkgname, vpzname=vpzname,
                           storaged_list=storaged_list,
                           conditions=conditions,
                           begin=begin, duration=duration)

        except Exception as e :
            msg = "[ExperimentViewMixin][modify_experiment]"
            cnt = build_error_content(exception=e, errormsg=msg)
            raise Exception(str(cnt))

        return vpzact

    def action_experiment_post(self, request):
        """action done for a POST request about experiment of a vpz"""

        data = request.data
        obj = request.obj
        try :
            self.to_log("POST vpz/experiment", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            (cr_tmp, restype_value, plan_value, begin_value, duration_value,
             parameters, parselect_values, outselect_values) = \
            self.getExperimentValues(data=data, vpzact=vpzact)
        except :
            raise
        if not cr_tmp :
            errormsg = "Unable to satisfy the request"
            raise Exception(errormsg)
        try :
            res = self.modify_experiment(vpzact=vpzact,
                                         begin_value=begin_value,
                                         duration_value=duration_value,
                                         parameters=parameters,
                                         outselect_values=outselect_values)
            # parselect
            DATA = {'parselect':parselect_values}
            vpzinput = self.init_input(data=DATA, vpzact=vpzact)
        except :
            raise
        vpzact = vpzinput.vpzact
        try :
            self.update_datafolder(data=data, vpzact=vpzact)
        except :
            raise
        try :
            vpzoutput = self.simulate(plan=plan_value, restype=restype_value,
                                      vpzact=vpzact)
        except :
            raise
        return (vpzinput, vpzoutput)

    def action_experiment_get(self, request):
        """action done for a GET request about experiment of a vpz

           Note WS : on pourrait prendre en compte une option parselect issue de la requete (actuellement parselect ne fait pas partie des options de GET vpz/experiment) en recuperant parselect dans data au lieu de le forcer a "all".
        """

        data = request.data
        obj = request.obj
        try :
            self.to_log("GET vpz/experiment", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            # "outselect"="all" and "parselect" = "all"
            res = self.modify_experiment(vpzact=vpzact,
                                         outselect_values=["all"])
            DATA = {'parselect':'all'}
            vpzinput = self.init_input(data=DATA, vpzact=vpzact)
        except :
            raise
        vpzact = vpzinput.vpzact
        return (vpzinput, None)

    def build_experiment_out_folder(self, vpzact, vpzinput, vpzoutput) :
        """builds content of the report file (into a folder) about experiment

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(LOGGER=self.config.LOGGER,
                               vpzinput=vpzinput, vpzoutput=vpzoutput,
                               with_general_output=True,
                               default_general_output=False,
                               with_out_ident_and_nicknames=True,
                               with_res=True, with_bycol=False)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        outputfile_name = self.config.experiment_filename
        vpzact.make_experiment_out_file_xls(dirpath=path,
                                            filename=outputfile_name,
                                            vpzoutput=vpzoutput)
        return outputfile_name

    def build_experiment_in_folder(self, vpzact, vpzinput, vpzoutput) :
        """builds content of the report file (into a folder) about experiment

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(LOGGER=self.config.LOGGER,
                               vpzinput=vpzinput, vpzoutput=vpzoutput,
                               with_general_output=True,
                               default_general_output=True,
                               with_out_ident_and_nicknames=True,
                               with_res=False, with_bycol=False)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        outputfile_name = self.config.experiment_filename
        vpzact.make_experiment_in_file_xls(dirpath=path,
                                           filename=outputfile_name)
        return outputfile_name

