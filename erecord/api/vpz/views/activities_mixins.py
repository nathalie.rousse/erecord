# -*- coding: UTF-8 -*-
"""erecord.api.vpz.views.activities_mixins

Mixins common to vpz application views (activities)

"""

import datetime

from rest_framework.reverse import reverse

from erecord.api.vpz.models import VpzAct
from erecord.api.vpz.models import VpzInput
from erecord.api.vpz.models import VpzOutput
from erecord.api.vpz.models import VpzWorkspace

from erecord.api.cmn.views_mixins import ModeOptionViewMixin
from erecord.api.cmn.views_mixins import DataFolderCopyViewMixin

from erecord.api.vpz.compact.views_mixins import VleParCompactOptionViewMixin

from erecord.api.vpz.serializers import VpzActivityDetailSerializer
from erecord.api.cmn.serializers import getKeyOptionValues

from erecord.api.vpz.compact.serializers import VpzInputCompact
from erecord.api.vpz.compact.serializers import VpzInputCompactSerializer

from erecord.api.cmn.utils.dir_and_file import unzip_file

#from erecord_cmn.utils.coding import byteify
#from erecord.api.cmn.utils.coding import get_val

from erecord.api.cmn.utils.erecord_package import vle_modify_vpz

#------------------------------------------------------------------------------

class OutselectOptionViewMixin(object):
    """additional methods for views having outselect option
    
    The 'outselect' option is used to choose restituted output datas.
 
    Value "all" : to select all output datas of all views
    Value viewname : to select all output datas of the view named viewname
    Value vname.oname : to select the ouput data oname of the view named \
    vname (and unselect the out datas of this view that are not selected \
    like this)

    Attention :
    The selection of type vname.oname is available in 'dataframe' restype \
    case. It won't be completely applied in 'matrix' restype case, even if it \
    has an impact on the view named vname.
    """

    def get_outselect_option_values(self, data):
        """Option 'outselect' (several values are possible)
    
        Returns the list of outselect option values.
    
        The 'outselect' option is used to choose restituted output datas.
        'outsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='outselect')) | 
                set(getKeyOptionValues(data=data, key='outsel')))
        outselect_option = None
        if len(s) > 0:
            outselect_option = s
        return outselect_option

class ParselectOptionViewMixin(object):
    """additional methods for views having parselect option
    
    The 'parselect' option is used to choose restituted parameters.
 
    Value "all" : to select all parameters of all conditions
    Value condname : to select all parameters of the condition named condname
    Value cname.pname : to select the parameter pname of the condition named \
    cname (and unselect the parameters of this condition that are not \
    selected like this)
    """

    def get_parselect_option_values(self, data):
        """Option 'parselect' (several values are possible)
    
        Returns the list of parselect option values.
    
        The 'parselect' option is used to choose restituted parameters.
        'parsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='parselect')) | 
                set(getKeyOptionValues(data=data, key='parsel')))
        parselect_option = None
        if len(s) > 0:
            parselect_option = s
        return parselect_option

class VpzInputCompactViewMixin(OutselectOptionViewMixin,
         ParselectOptionViewMixin, VleParCompactOptionViewMixin):
    """additional methods for views having options to modify VpzInput """

    def getVpzInputCompactOptionValues(self, data):
        """Options to modify vpz file (indirectly future read VpzInput)
        
        Returns (cr_ok, vpzinputcompact, outselect_option, pars) \
        that are relative to VpzInputCompactSerializer, \
        get_outselect_option_values and get_vleparcompact_option_values).

        Note :
        parselect_option (relative to get_parselect_option_values) is not 
        treated here ; it modifies VpzInput directly, not through
        modification of vpz file.
        """

        vpzinputcompact = VpzInputCompact()
        serializer = VpzInputCompactSerializer(instance=vpzinputcompact,
                                               data=data)
        cr_ok = serializer.is_valid()
        if cr_ok :
            serializer.save() # => vpzinputcompact updated
            vpzinputcompact = serializer.update_pars(instance=vpzinputcompact,
                                                     data=data)
            outselect_option = self.get_outselect_option_values(data=data)
            vleparcompact_option = self.get_vleparcompact_option_values(
                                                                   data=data)
            ###parselect_option = self.get_parselect_option_values(data=data)
        else :
            begin_value = None
            duration_value = None
            outselect_option = None
            vleparcompact_option = None
            ###parselect_option = None
        return (cr_ok, serializer, vpzinputcompact, outselect_option,
                vleparcompact_option)

class DataFolderViewMixin(DataFolderCopyViewMixin):
    """additional methods for views where 'data' folder can be modified
    
    The 'data' folder of the main vle package can be modified.

    Option datafolder : zip file containing the new 'data' folder to be taken
    into account. Something else than the 'data' folder that may be contained
    into the datafolder zip file won't be considered. 

    Option datafoldercopy : the way how to take into account datafolder.
    - Value replace : the content of (the 'data' folder contained into) the
      datafolder zip file replaces the original 'data' folder.
    - Value overwrite : the content of (the 'data' folder contained into) the
      datafolder zip file is added to the original 'data' folder (by
      overwriting).
    """

    def update_datafolder(self, data, vpzact) :
        """updates the 'data' folder

        Updates the 'data' folder of the main vle package according to the
        datafolder option (zip file containing 'data' folder) and the
        datafoldercopy option (replace or overwrite).

        datafolder is supposed to contain a zip file.
        datafolder values ignored : "", None.
        else : raise error
        """

        if 'datafolder' in data.keys() :

            if data['datafolder'] == "":
                return
            if data['datafolder'] is None :
                return

            cr_ok = False # default
            w = vpzact.vpzworkspace
            w.clean_datahome()
            try :
                #WS
                #zip_file_name = data['datafolder']
                #zip_file_path = w.get_datafolder_zip_file_path(zip_file_name)
                zip_file_path = data['datafolder']

                cr_ok = unzip_file(zip_file_path=zip_file_path,
                                   path=w.get_datahome())
            except :
                raise
            try :
                datafoldercopy = self.get_datafoldercopy_value(data=data)
                self.modify_datafolder(datafoldercopy=datafoldercopy,
                                       vpzact=vpzact)
            except :
                raise

    def modify_datafolder(self, datafoldercopy, vpzact):
        """updates the 'data' folder

        Updates the 'data' folder of the main vle package according to the
        datafoldercopy option and the 'data' folder found under datahome.

        Raise error if no 'data' folder found under datahome.
        """

        w = vpzact.vpzworkspace
        (cr_ok, datafolder_src_path, data_name) = \
                                       w.get_and_verify_datafolder_src_path()
        if not cr_ok :
            errormsg = "Bad request : folder named '"+data_name+"' not found "
            errormsg += "into the zip file received by the datafolder option. "
            errormsg += "This zip file must contain a folder "
            errormsg += "named '"+data_name+"' that contains all the data "
            errormsg += "files to be taken into account."
            raise Exception(errormsg)
        try :
            if datafoldercopy == 'overwrite' :
                w.overwrite_pkg_data(data_src=datafolder_src_path,
                                     vle_version=vpzact.vleversion,
                                     pkgname=vpzact.pkgname)
            else : # 'replace', default
                w.replace_pkg_data(data_src=datafolder_src_path,
                                   vle_version=vpzact.vleversion,
                                   pkgname=vpzact.pkgname)
        except :
            raise

class ActivityViewMixin(VpzInputCompactViewMixin, ModeOptionViewMixin):
    """additional methods for activities views

       NB : uses self.config
    """

    def getVpzActivityDetailOptionValues(self, data):
        """Options to call a vpz activity (relative to a VpzAct)
    
        returns a dict"""
    
        #... et 'outselect' 'parselect' ?

        options = dict()
        p = VpzActivityDetailSerializer(data=data)
        p.is_valid()
        for k,v in p.data.items() :
            if v is not None :
                options[k] = v
        mode_options = self.get_mode_option_values(data=data)
        if mode_options is not None :
            options['mode'] = mode_options
        report_option = self.get_report_option_values(data=data)
        if report_option is not None :
            options['report'] = report_option
        return options

    def to_log(self, url_txt, data) :
        """Records activity event into activity log file"""

        if self.config.LOG_ACT_ACTIVE :
            try :
                text = str(datetime.datetime.now())
                text += " -R- " + url_txt
                text += " -D- " + str(data) + "\n"
                f = open(self.config.LOG_ACT_FILE, "a")
                f.write(text)
                f.close()
            except :
                pass

    def init_activity(self, obj):
        """initial build
        
        Builds VpzAct, VpzWorspace

        Receives and controls token in case of locked vpz (vpz_path or vlevpz).

        Returns VpzAct if no error, and raises exception else.

        Note : VpzInput and VpzOutput are not built there
        """

        try:
            vpzact = VpzAct.create(vpzname=obj['vpzname'],
                                   pkgname=obj['pkgname'], 
                                   vlepath=obj['vlepath'],
                                   vleversion=obj['vleversion'],
                                   vleusrpath=obj['vleusrpath'])
            vpzact.control()

        except:
            raise

        # vlehome, reporthome, datahome, runhome subworkspaces are created
        try :
            vpzact.vpzworkspace = VpzWorkspace.create(config=self.config,
                                        as_vlehome=True,
                                        as_reporthome=True, as_datahome=True,
                                        as_runhome=True)
            vlepackage_list = vpzact.get_ordered_vlepackage_list()
            vpzact.vpzworkspace.build_pkg_and_its_dependencies(
                   vpzname=vpzact.vpzname, vlepackage_list=vlepackage_list,
                   vle_version=vpzact.vleversion)
            vpzact.vpzworkspace.control()
        except:
            raise

        return vpzact

    def init_input(self, data, vpzact):
        """build VpzInput

        Builds VpzInput from vpz file (relative to vpzact) and takes into \
        account parselect option.

        Returns VpzAct if no error, and raises exception else.

        Note : data options, other than parselect \
        (see getVpzInputCompactOptionValues), are taken into account \
        directly into vpz file (previously modified).
        """

        try :
            vpzinput = VpzInput(vpzact)
            vpzinput.read_and_add_content()
            parselect_option = self.get_parselect_option_values(data=data)
            vpzinput.update_vlepar_selected_and_save(
                                                   parselect=parselect_option)
        except:
            raise

        return vpzinput

    def modify_input_vpz(self, data, vpzact):
        """Modifies the vpz file (relative to vpzact) according to data.

        data information can correspond to VpzInputCompactSerializer format,
        where parameters are given in 'pars'. Each parameter 
        ('cname', 'pname', 'value') can also be given as the data 
        'pname'.'cname' with value 'value'. If a parameter value was given 
        in both ways, there is no guarantee about which one would be kept.

        data information can contain the 'outselect' option that is used
        to select the restituted output datas (see OutselectOptionViewMixin :
        all, view, output data...). This 'outselect' option is applied to the
        vpz file.

        data information can contain the 'parselect' option that is used
        to select the restituted parameters (see ParselectOptionViewMixin :
        all, cond, parameter...). This 'parselect' option is NOT applied to
        the vpz file, it will then be applied directly on VpzInput.

        Returns VpzAct if no error, and raises exception else.

        Uses modify_vpz simulator of erecord package.

        The vpz file (relative to vlehome of vpzact.vpzworkspace) is modified.
        """

        (cr_tmp, serializer, vpzinputcompact, outselect_option,
         vleparcompact_option) = self.getVpzInputCompactOptionValues(data=data)

        if cr_tmp :

            try :
                vlehome_path = vpzact.vpzworkspace.get_vlehome()
                runvle_path = vpzact.vpzworkspace.define_and_build_runhome_subdirectory(rootname="modify_")
                vle_version = vpzact.vleversion
                vle_usr_path = vpzact.vleusrpath
                pkgname = vpzact.pkgname
                vpzname = vpzact.vpzname

                # builds storaged_list, conditions, begin, duration from data

                storaged_list = outselect_option

                conditions = dict()
                for par in vleparcompact_option :
                    cname = par.cname
                    pname = par.pname
                    value = par.value #WS value = get_val(par.value)
                    if cname not in conditions.keys():
                        conditions[cname] = dict()
                    conditions[cname][pname] = value
                for par in vpzinputcompact.pars :
                    cname = par.cname
                    pname = par.pname
                    value = par.value #WS value = get_val(par.value)
                    if cname not in conditions.keys():
                        conditions[cname] = dict()
                    conditions[cname][pname] = value

                begin = vpzinputcompact.begin
                duration = vpzinputcompact.duration

                vle_modify_vpz(runvle_path=runvle_path,
                               vlehome_path=vlehome_path,
                               vle_version=vle_version,
                               vle_usr_path=vle_usr_path,
                               pkgname=pkgname, vpzname=vpzname,
                               storaged_list=storaged_list,
                               conditions=conditions,
                               begin=begin, duration=duration)
            except :
                raise

        else :
            errormsg = "Bad request : " + str(serializer.errors)
            raise Exception(errormsg)

        return vpzact


    def simulate(self, plan, restype, vpzact):
        """Runs simulation (relative to vpzact) according to plan and restype.
        
        Runs simulation and builds vpzact.vpzoutput.

        Returns VpzAct if no error, and raises exception else.

        Uses run_vpz simulator of erecord package.
        """

        vpzoutput = VpzOutput(vpzact)
        try :
            vpzoutput.run_and_add_content(plan=plan, restype=restype)
        except :
            raise

        return vpzoutput

class InputViewMixin(ActivityViewMixin):
    """additional methods for activities views about input of a vpz"""

    def action_input_get(self, request):
        """action done for a GET request about input of a vpz"""

        data = request.data
        obj = request.obj
        try :
            self.to_log("GET vpz/input", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            vpzinput = self.init_input(data=data, vpzact=vpzact)
        except :
            raise
        return vpzinput

    def action_input_post(self, request):
        """action done for a POST request about input of a vpz"""

        data = request.data
        obj = request.obj
        try :
            self.to_log("POST vpz/input", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            vpzact = self.modify_input_vpz(data=data, vpzact=vpzact)
            vpzinput = self.init_input(data=data, vpzact=vpzact)
        except :
            raise
        return vpzinput

class OutputViewMixin(DataFolderViewMixin, ActivityViewMixin):
    """additional methods for activities views about output of a vpz"""

    def action_output_get(self, request):
        """action done for a GET request about output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.
        """

        data = request.data
        obj = request.obj
        try : 
            self.to_log("GET vpz/output", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try : 
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            vpzoutput = self.simulate(plan=plan, restype=restype,
                                      vpzact=vpzact)
        except :
            raise
        return vpzoutput

    def action_output_post(self, request):
        """action done for a POST request about output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.
        """

        data = request.data
        obj = request.obj
        try :
            self.to_log("POST vpz/output", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            vpzact = self.modify_input_vpz(data=data, vpzact=vpzact)
        except :
            raise
        try :
            self.update_datafolder(data=data, vpzact=vpzact)
        except :
            raise
        try :
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            vpzoutput = self.simulate(plan=plan, restype=restype,
                                      vpzact=vpzact)
        except :
            raise
        return vpzoutput

class InOutputViewMixin(DataFolderViewMixin, ActivityViewMixin):
    """additional methods for activities views about input,output of a vpz"""

    def action_inoutput_get(self, request):
        """action done for a GET request about input and output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.
        """

        data = request.data
        obj = request.obj
        try : 
            self.to_log("GET vpz/inout", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            vpzinput = self.init_input(data=data, vpzact=vpzact)
        except :
            raise
        vpzact = vpzinput.vpzact
        try : 
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            vpzoutput = self.simulate(plan=plan, restype=restype,
                                      vpzact=vpzact)
        except :
            raise
        return (vpzinput, vpzoutput)

    def action_inoutput_post(self, request):
        """action done for a POST request about input and output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.
        """

        data = request.data
        obj = request.obj
        try :
            self.to_log("POST vpz/inout", data)
            vpzact = self.init_activity(obj=obj)
        except :
            raise
        try :
            vpzact = self.modify_input_vpz(data=data, vpzact=vpzact)
            vpzinput = self.init_input(data=data, vpzact=vpzact)
        except :
            raise
        vpzact = vpzinput.vpzact
        try :
            self.update_datafolder(data=data, vpzact=vpzact)
        except :
            raise
        try :
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            vpzoutput = self.simulate(plan=plan, restype=restype,
                                      vpzact=vpzact)
        except :
            raise
        return (vpzinput, vpzoutput)

