# -*- coding: UTF-8 -*-
"""api.vpz.views.activities

vpz application views (activities)

"""

import os

from erecord.api.cmn.views_mixins import StyleViewMixin
from erecord.api.cmn.views_mixins import DataViewMixin
from erecord.api.cmn.views_mixins import VpzIdentViewMixin
from erecord.api.cmn.views_mixins import ActionViewMixin
from erecord.api.vpz.views.activities_mixins import ActivityViewMixin
from erecord.api.vpz.views.activities_mixins import InputViewMixin
from erecord.api.vpz.views.activities_mixins import OutputViewMixin
from erecord.api.vpz.views.activities_mixins import InOutputViewMixin
from erecord.api.vpz.views.views_mixins import ReportViewMixin
from erecord.api.vpz.views.views_mixins import ExperimentViewMixin

from erecord.api.vpz.views.objects_mixins import VpzInputDetailViewMixin
from erecord.api.vpz.views.objects_mixins import VpzOutputDetailViewMixin
from erecord.api.vpz.views.objects_mixins import VpzInOutDetailViewMixin

from erecord.api.cmn.utils.errors import build_error_content

class InputView(VpzInputDetailViewMixin, InputViewMixin,
                StyleViewMixin, DataViewMixin):
    """input information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_input`, :ref:`post_vpz_input`).
    
    Returns VpzInput information of a VpzAct after having created it and 
    maybe, in POST command case, has modified before the vpz file relative 
    to VpzAct.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or 
    kept into the database depending on persistence management.
    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'

    def get(self, request): # previous GET vpz/input
        """Returns VpzInput."""

        self.method = 'GET'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def post(self, request): # previous POST vpz/input
        """Modifies vpz file relative to vpzact according to post data 
           and returns VpzInput.
        """

        self.method = 'POST'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def numeric_response(self, style):
        """response about VpzInput in numeric case

        Looks like VpzInputDetail
        """
        # format available values : 'json' 'yaml' 'xml' (no 'html', no 'api')
        # style available values : 'compact' 'compactlist' 'tree' (no 'link')

        try :
            if self.method == 'POST' :
                vpzinput = self.action_input_post(request=self.request)
            else : # 'GET'
                vpzinput = self.action_input_get(request=self.request)
            context = self.context(vpzinput=vpzinput, style=style)
        except Exception as e :
            errormsg= "[InputView][numeric_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

class OutputView(VpzOutputDetailViewMixin, OutputViewMixin,
                 StyleViewMixin, DataViewMixin):
    """output information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_output`, :ref:`post_vpz_output`).
    
    Returns VpzOutput information of a VpzAct after having created it and 
    done the required operations : maybe has modified its vpz file 
    (only in POST command case), has run simulation.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or 
    kept into the database depending on persistence management.
    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'

    def get(self, request): # previous GET vpz/output
        """Simulates vpz file relative to vpzact and returns VpzOutput."""

        self.method = 'GET'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def post(self, request): # previous POST vpz/output
        """Modifies vpz file relative to vpzact according to post data, 
           simulates it and returns VpzOutput.
        """

        self.method = 'POST'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def numeric_response(self, style):
        """response about VpzOutput in numeric case

        Looks like VpzOutputDetail

        """
        # format available values : 'json' 'yaml' 'xml' (no 'html', no 'api')
        # style available values : 'compact' 'compactlist' 'tree' (no 'link')

        try :
            if self.method == 'POST' :
                vpzoutput = self.action_output_post(request=self.request)
            else : # 'GET'
                vpzoutput = self.action_output_get(request=self.request)
            context = self.context(vpzoutput=vpzoutput, style=style)
        except Exception as e :
            errormsg= "[OutputView][numeric_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

class InOutView(VpzInOutDetailViewMixin, InOutputViewMixin,
                StyleViewMixin, DataViewMixin):
    """output information of a vpz and input information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_inout`, :ref:`post_vpz_inout`).
    
    Returns VpzInput and VpzOutput information of a VpzAct after having
    created it and done the required operations : maybe has modified its 
    vpz file (only in POST command case), has read its VpzInput information,
    has run simulation.

    In fact in the current version the returned information contains more
    than VpzOutput and VpzInput, it contains all VpzAct information.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or
    kept into the database depending on persistence management.
    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'


    def get(self, request): # previous GET vpz/inout
        """Simulates vpz file relative to vpzact, returns 
           VpzInput and VpzOutput.
        """

        self.method = 'GET'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def post(self, request): # previous POST vpz/inout
        """Modifies vpz file relative to vpzact according to post data, 
           simulates it and returns VpzInput and VpzOutput.
        """

        self.method = 'POST'
        self.request = request
        style = self.get_style_value(data=self.request_data(request))
        context = self.numeric_response(style=style)
        return context

    def numeric_response(self, style):
        """response about VpzAct in numeric case

        Looks like (at least) VpzInputtDetail + VpzOutputDetail
        """

        # format available values : 'json' 'yaml' 'xml' (no 'html', no 'api')
        # style available values : 'compact' 'compactlist' 'tree' (no 'link')

        try :
            if self.method == 'POST' :
                (vpzinput, vpzoutput) = self.action_inoutput_post(
                                                          request=self.request)
            else : # 'GET'
                (vpzinput, vpzoutput) = self.action_inoutput_get(
                                                          request=self.request)
            vpzact = vpzinput.vpzact
            context = self.context(vpzinput=vpzinput, vpzoutput=vpzoutput,
                                   vpzact=vpzact, style=style)
        except Exception as e :
            errormsg= "[InOutView][numeric_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

class ReportView(ReportViewMixin, InOutputViewMixin,
                 StyleViewMixin, DataViewMixin):
    """output and input information of a vpz as report(s)

    NB : uses self.config

    For more information, see the online documentation
    (:ref:`get_vpz_report`, :ref:`post_vpz_report`).
    
    Creates VpzInput and VpzOutput information of a VpzAct by doing the 
    required operations : maybe has modified its vpz file (only in POST 
    command case), has run simulation.

    Produces and returns the required report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The 'report' option is used to choose which reports are built into the
    produced/returned folder. report values for (into the report folder) :

      - value 'vpz' : the vpz file.
      - value 'csv' : csv file(s).
      - value 'xls' : a xls file (several worksheets).
      - value 'txt' : txt file(s).
      - value 'all' : all the available files above (txt, csv...).
 
    The 'bycol' mode option is used to choose to have some arrays by columns 
    (by default : by rows).

    NO : The 'todownload' mode option is used to choose to download the result.

    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'

    def get(self, request): # previous GET vpz/report
        """Simulates vpz file relative to vpzact, returns VpzOutput 
           and VpzInput into a report folder.
        """

        self.method = 'GET'
        self.request = request
        data = self.request_data(self.request)
        reports = self.get_report_values(data=data)
        bycol = self.get_bycol_value(data=data)
        context = self.report_response(reports=reports, bycol=bycol)
        return context

    def post(self, request): # previous POST vpz/report
        """Modifies vpz file relative to vpzact according to post data, 
           simulates it, returns VpzInput and VpzOutput into a report folder.
        """

        self.method = 'POST'
        self.request = request
        data = self.request_data(self.request)
        reports = self.get_report_values(data=data)
        bycol = self.get_bycol_value(data=data)
        context = self.report_response(reports=reports, bycol=bycol)
        return context

    def report_response(self, reports, bycol):
        """response about VpzAct"""

        try :
            if self.method == 'POST' :
                (vpzinput, vpzoutput) = self.action_inoutput_post(
                                                          request=self.request)
            else : # 'GET'
                (vpzinput, vpzoutput) = self.action_inoutput_get(
                                                          request=self.request)
            vpzact = vpzinput.vpzact
            vpzact.vpzworkspace.clean_reporthome()
            self.build_folder(vpzact=vpzact,
                              vpzinput=vpzinput, vpzoutput=vpzoutput,
                              reports=reports, bycol=bycol)
            title = "Report .zip file"
            txt = "The report .zip file content depends "
            txt = txt + " on 'report' option values"
            context = {'desc':title, 'help':txt}

            zip_folder_path = self.build_zip_folder(vpzact=vpzact) # required
            context['zip_file_path'] = zip_folder_path

            outputfile_name = self.config.report_filename
            report_file_path = zip_folder_path
            context['outputfile_path'] = report_file_path
            context['outputfile_name'] = outputfile_name

        except Exception as e :
            errormsg= "[ReportView][report_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

class ReportConditionsView(ReportViewMixin, InputViewMixin,
                 StyleViewMixin, DataViewMixin):
    """input information of a vpz as report(s)

    For more information, see the online documentation
    (:ref:`get_vpz_report_conditions`, :ref:`post_vpz_report_conditions`).
    
    Creates VpzInput information of a VpzAct by doing the required 
    operations : maybe has modified the vpz file relative to VpzAct (only in 
    POST command case), reads VpzInput. Produces and returns the required 
    report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The report produced/returned is xls files, about simulation conditions.

    The 'bycol' mode option is used to choose to have some arrays by columns 
    (by default : by rows).

    NO : The 'todownload' mode option is used to choose to download the result.
 
    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'

    def get(self, request): # previous GET vpz/report/conditions
        """Reads VpzInput, returns simulation conditions (from VpzInput) 
           into a report folder.
        """

        self.method = 'GET'
        self.request = request
        data = self.request_data(self.request)
        bycol = self.get_bycol_value(data=data)
        context = self.report_response(bycol=bycol)
        return context

    def post(self, request): # previous POST vpz/report/conditions
        """Modifies the vpz file relative to vpzact according to post data, 
           reads VpzInput, returns simulation conditions (from VpzInput) 
           into a report folder.
        """

        self.method = 'POST'
        self.request = request
        data = self.request_data(self.request)
        bycol = self.get_bycol_value(data=data)
        context = self.report_response(bycol=bycol)
        return context

    def report_response(self, bycol):
        """response about simulation conditions (from VpzInput)"""

        try :
            if self.method == 'POST' :
                vpzinput = self.action_input_post(request=self.request)
            else : # 'GET'
                vpzinput = self.action_input_get(request=self.request)
            vpzact = vpzinput.vpzact
            vpzact.vpzworkspace.clean_reporthome()
            outputfile_name = self.build_conditions_folder(vpzact=vpzact,
                                               vpzinput=vpzinput, bycol=bycol)
            title = "Conditions report .xls file"
            context = {'desc':title}
            # NOT necessary
            #zip_folder_path = self.build_zip_folder(vpzact=vpzact)
            #context['zip_file_path'] = zip_folder_path
            folder_path = self.get_folder_path(vpzact=vpzact)
            conditions_file_path = os.path.join(folder_path, outputfile_name)
            context['outputfile_path'] = conditions_file_path
            context['outputfile_name'] = outputfile_name

        except Exception as e :
            errormsg= "[ReportConditionsView][report_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

class ExperimentView(ExperimentViewMixin, ActivityViewMixin,
                     StyleViewMixin, DataViewMixin):
    """experiment of a vpz, into a xls file

    In GET case, the returned xls file contains the experiment conditions 
    in their original state.  

    In POST case, the returned xls file contains the simulation results and 
    the experiment conditions that have been modified according to the xls 
    file given by the user into the POST request.
    
    The posted xls file (POST vpz/experiment request) has the same format 
    and structure than the xls file returned by GET vpz/experiment request.

    For more information, see the online documentation
    (:ref:`get_vpz_experiment`, :ref:`post_vpz_experiment`).

    The VpzAct is created from a VpzPath or from a VleVpz.

    NO : The 'todownload' mode option is used to choose to download the result.

    """

    def __init__(self, config):

        self.config = config
        self.request = None
        self.method = None   # available values : 'GET','POST'

    def get(self, request): # previous GET vpz/experiment
        """experiment conditions of a vpz, into a xls file

        The returned xls file contains the experiment conditions (input
        information) : begin, duration and parameters values in their
        original state, information to identify all the existing vlecond, 
        vlepar, vleview, vleout (forcing all the parameters and output datas 
        identification : outselect=all, parselect=all).  

        Note : the returned file will be useful to build POST requests.
        """

        self.method = 'GET'
        self.request = request
        context = self.experiment_response()
        return context

    def post(self, request): # previous POST vpz/experiment
        """experiment conditions and results of a vpz, into a xls file

        Modifies VpzInput according to POST FILE data (content of the xls 
        file given by the user into the POST request), simulates it and 
        returns input and output information (from VpzInput, VpzOutput) into 
        a xls file.

        The returned xls file contains the simulation results (output 
        information) and the experiment conditions (input information) : 
        begin, duration and parameters values modified according to POST FILE 
        data. Only the selected vlecond, vlepar, vleview, vleout are returned. 

        Note :
        The xls file given by the user into the POST request has the same 
        format and structure than the xls file returned by a 
        GET vpz/experiment request. To build the POSTed file, 
        GET vpz/experiment may help.
        """

        self.method = 'POST'
        self.request = request
        context = self.experiment_response()
        return context

    def experiment_response(self):
        """response about experiment (from VpzAct)"""

        outputfile_name = "output_file" # default

        try :
            if self.method == 'POST' :
               (vpzinput, vpzoutput) = self.action_experiment_post(
                                                         request=self.request)
            else : # 'GET'
                (vpzinput, vpzoutput) = self.action_experiment_get(
                                                         request=self.request)
            vpzact = vpzinput.vpzact
            vpzact.vpzworkspace.clean_reporthome()

            if self.method == 'POST' :
                outputfile_name = self.build_experiment_out_folder(
                                      vpzact=vpzact,
                                      vpzinput=vpzinput, vpzoutput=vpzoutput)
                title = "Experiment (conditions and results) .xls file"
                context = {'desc':title}
            else : # 'GET'
                outputfile_name = self.build_experiment_in_folder(
                                      vpzact=vpzact,
                                      vpzinput=vpzinput, vpzoutput=vpzoutput)
                title = "Experiment (conditions) .xls file"
                txt = "Use the .xls experiment file "
                txt = txt + " as input for POST vpz/experiment request"
                context = {'desc':title, 'help':txt}
            # NOT necessary
            #zip_folder_path = self.build_zip_folder(vpzact=vpzact)
            #context['zip_file_path'] = zip_folder_path
            folder_path = self.get_folder_path(vpzact=vpzact)
            experiment_file_path = os.path.join(folder_path, outputfile_name)
            context['outputfile_path'] = experiment_file_path
            context['outputfile_name'] = outputfile_name

        except Exception as e :
            errormsg= "[ExperimentView][experiment_response]"
            context = build_error_content(exception=e, errormsg=errormsg)
        return context

