# -*- coding: UTF-8 -*-
"""
    erecord.api.erecord

"""

#import django
#print("[erecord]", "django ", django.get_version())

# SETTINGS
from django.conf import settings
settings.configure(DEBUG=False)

from erecord.api.cmn.configs.config import Config

from erecord.api.cmn.utils.dir_and_file import create_dir_if_absent
from erecord.api.cmn.utils.dir_and_file import clean_dir

from erecord.api.cmn.urls import RequestAction
from erecord.api.cmn.urls import RequestError

PM = "[erecord]"

def init(config):
    """Initialisation

       Uses (controls, modifies) configuration =>
       to be called after having defined configuration :
       calling 'conf' method, maybe followed by modifications

    """

    try:
        # Folders 

        create_dir_if_absent(config.PROJECT_HOME)

        for path in (config.factory_path, config.RUN_HOME, config.LOG_FOLDERPATH, config.out_path):
            (deletion_done, creation_done) = clean_dir(dirpath=path)
            if not creation_done:
               errormsg = PM+"[init]"+" "
               errormsg += "Has not created the required "+path+" folder"
               raise Exception(errormsg)

        # Log
        config.set_LOGGER()

    except:
        raise
    return "OK"

def conf():
    """Configuration"""

    configuration = Config()
    return configuration

def action(config, data) :
    """To ask for launching an action and returning its result as response"""

# GET vpz/input to see a simulator (vpz)
# POST vpz/input to see a modified simulator (vpz)

# GET vpz/output to run a simulator (vpz)
# POST vpz/output to run a modified simulator (vpz)

# GET vpz/inout  to see and run a simulator (vpz)
# POST vpz/inout to see and run a modified simulator (vpz)

# GET vpz/experiment to see a simulator (vpz) by xls file  ('first step')
# POST vpz/experiment to run a modified simulator (vpz) by xls file

# GET vpz/report  for reports about a simulator (vpz) (conditions and results)
# POST vpz/report for reports about a modified simulator (vpz) (conditions and results)

# GET vpz/report/conditions for reports about a simulator (vpz) conditions
# POST vpz/report/conditions for reports about a modified simulator (vpz) conditions

# GET db/pkg to identify some models (pkg)
# GET db/pkg/{Id} to identify a model (pkg)
# GET db/vpz to identify some simulators (vpz)
# GET db/vpz/{Id} to identify a simulator (vpz)

# GET db/pkg/{Id}/name to get the name of a model (pkg)
# GET db/pkg/{Id}/datalist to get the list of names of data files of a model (pkg)

    try :
        request = RequestAction(config, data)
        response = request.response()
    except :
        raise
    return response

def error_case(data, msg, e):
    """Builds and returns response in error case

    (from error message and exception)
    """

    request = RequestError(data)
    response = request.response(msg, e)
    return response

