# get_vpz_inout request

# Description

Returns the simulation results of a simulator (vpz) 
after having run simulation, and returns also its input conditions.

# Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)
- 'style' : [more](./requests.md)
  - Available values : only 'tree' ('compact', 'compactlist' : unavailable).

## Example

TODO

