# get_vpz_experiment request

## Description

Returns a .xls file (as 'experiment' output) containing the 
experiment conditions of a simulator (vpz).

## Note

Such a 'get_vpz_experiment' request may be useful
before a 'post_vpz_experiment' request : see 'Help' of 
['post_vpz_experiment'](./request_post_vpz_experiment.md) request.

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)

## Response

The returned .xls file ('experiment' output)
contains the experiment conditions of the simulator :

  - General information :
    - plan *(with a default value among
      the 'single' and 'linear' available values)*,
    - restype *(with a default value among
      the 'dataframe' and 'matrix' available values)*,
    - begin, duration.
  - Parameters values : list of all the parameters values.
  - Parameters identification : list of all the parameters.
  - Output datas identification : list of the output datas.

**Limit** : in case of xls file overflow
*(sheets limited to 256 columns and 65536 rows)* the returned .xls file
('experiment' output) will be truncated (without any warning).

## Example

**erecord_file tool case**

- Inputs :

  - action_name = 'get_vpz_experiment'

  - input_json (containing the request) :

    ```
    {"pkgname":"wwdm", "vpzname":"wwdm.vpz"}
    ```

- Outputs :

  - output_json

  - experiment : [experiment.xls](./outputs/Galaxy3-[experiment].xls) file
    (containing the experiment plan).

Note : the resulting  [experiment.xls](./outputs/Galaxy3-[experiment].xls)
file can then be used (after having been modified or not)
as 'experimentfile' input of a
['post_vpz_experiment'](./request_post_vpz_experiment.md) request.

