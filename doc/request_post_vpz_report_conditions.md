# post_vpz_report_conditions request

## Description

Maybe modifies the input conditions of a simulator (vpz)
(depending on the request options : see 'pars', 'begin'...).

Then produces and returns a .xls file (as 'conditions' output)
from the input conditions.

## Help

Before such a 'post_vpz_report_conditions' request, it may be useful to do a
'get_vpz_input' request with 'compact' or 'compactlist' values for 'style'
(see [more](./requests.md)), in order to make easier some modifications
(see 'pars', 'parselect', 'outselect' options).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'begin' : [more](./requests.md)
- 'duration' : [more](./requests.md)
- 'pars' or cname.pname : [more](./requests.md)
- 'parselect' : [more](./requests.md)
- 'outselect' : [more](./requests.md) ????
- 'mode' for 'bycol/byrow' : [more](./requests.md)

## Example

TODO

