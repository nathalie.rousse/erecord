# post_vpz_report request

In addition with the 'input_json' input containing this request,
there may be a 'datafolder' input (optional).

## Description

Maybe modifies the input conditions of a simulator (vpz)
(depending on the request options : see 'pars', 'begin'...).

In addition with this input conditions modification (optional),
maybe modifies the input datas folder of the simulator,
according to the 'datafolder' input (optional).

Then runs the simulation.

Produces required reports from the simulation results and input conditions.

Finally returns a .zip file (as 'report' output) gathering the built reports.

## Help

Before such a 'post_vpz_report' request, it may be useful to do a
'get_vpz_input' request with 'compact' or 'compactlist' values for 'style'
(see [more](./requests.md)), in order to make easier some modifications
(see 'pars', 'parselect', 'outselect' options).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)
- 'begin' : [more](./requests.md)
- 'duration' : [more](./requests.md)
- 'pars' or cname.pname : [more](./requests.md)
- 'datafoldercopy' : [more](./requests.md).
  Option in relation with the maybe joined 'datafolder' input.
- 'parselect' : [more](./requests.md)
- 'outselect' : [more](./requests.md)
- 'report' : [more](./requests.md)
- 'mode' for 'bycol/byrow' : [more](./requests.md)

## Example

## Example A.

**erecord_file tool case**

"report": "all" | "mode": "bycol"

- Inputs :

  - action_name = 'post_vpz_report'

  - input_json (containing the request) :

    ```
    {
      "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
      "plan": "linear", "restype": "dataframe", 
      "parselect": ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.K", "cond_wwdm.TI"], 
      "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
      "cond_wwdm.B": 0.00201, 
      "duration": 6.0, 
      "outselect": "all", 
      "report": "all", "mode": "bycol"
    }
    ```

- Outputs :

  - output_json
  - report : [report.zip](./outputs/report_all.zip) file (containing reports).

## Example B.

**erecord_file tool case**

"report": ["vpz", "csv", "xls"] | "byrow" default value

- Inputs :

  - action_name = 'post_vpz_report'

  - input_json (containing the request) :

    ```
    {
      "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
      "plan": "linear", "restype": "dataframe", 
      "parselect": ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.K", "cond_wwdm.TI"], 
      "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
      "cond_wwdm.B": 0.00201, 
      "duration": 6.0, 
      "outselect": "all", 
      "report": ["vpz", "csv", "xls"]
    }
    ```

- Outputs :

  - output_json
  - report : [report.zip](./outputs/Galaxy3-[report].zip) file
    (containing reports).

