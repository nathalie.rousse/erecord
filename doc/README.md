# erecord documentation

## General

The erecord python library allows to edit, modify and simulate some models developed with the VLE multi-modeling and simulation platform (http://www.vle-project.org).

The erecord python library contains some **python main programs** :
**main_json.py**, **main_file.py**.

Into erecord-deploy project, there are 2 corresponding **Galaxy tools**
(under galaxy-tools folder) :
  - erecord_json/**erecord_json.xml** (calling main_json.py).
  - erecord_file/**erecord_file.xml** (calling main_file.py).

In this documentation, we will use the same **"tool" term** for refering both
a "python main program" and its corresponding "Galaxy tool" :
  - **erecord_json** tool refering main_json.py and erecord_json.xml.
  - **erecord_file** tool refering main_file.py and erecord_file.xml.

**Tools Inputs/Outputs** :

  - Inputs :
    - erecord_json and erecord_file tools receive the request as a json data
      (see 'input_json' input).
    - They may receive some other input files, depending on the request.

  - Outputs :
    - erecord_json and erecord_file tools send a response as a json data :
      - erecord_json tool returns only this .json file.
      - for some specific actions, erecord_file tool returns also a 2nd file
        (.zip or .xls).

## Action

erecord_json and erecord_file tools can do different actions.

**Action values** :

  - *Information*

    - value 'help' : for help text
    - value ['get_pkg_list'](./request_get_pkg_list.md) :
      for list of existing Vle packages (pkg)
    - value ['get_pkg_vpz_list'](./request_get_pkg_vpz_list.md) :
      for simulators (vpz) list of a Vle package (pkg)
    - value ['get_pkg_data_list'](./request_get_pkg_data_list.md) :
      for data files list of a Vle package (pkg)

  - *See/run a simulator*

    - value ['get_vpz_input'](./request_get_vpz_input.md) :
      to see a simulator (vpz)
    - value ['post_vpz_input'](./request_post_vpz_input.md) :
      to see a modified simulator (vpz)
    - value ['get_vpz_output'](./request_get_vpz_output.md) :
      to run a simulator (vpz)
    - value ['post_vpz_output'](./request_post_vpz_output.md) :
      to run a modified simulator (vpz)
    - value ['get_vpz_inout'](./request_get_vpz_inout.md) :
      to both see and run a simulator (vpz)
    - value ['post_vpz_inout'](./request_post_vpz_inout.md) :
      to both see and run a modified simulator (vpz)

  - *Experiment plan by xls files*

    - value ['get_vpz_experiment'](./request_get_vpz_experiment.md) :
      to get by xls file and see the experiment plan of a simulator (vpz)
    - value ['post_vpz_experiment'](./request_post_vpz_experiment.md) :
      to send by xls file and 
      run the modified experiment plan of a simulator (vpz)

  - *Reports on a simulator*

    - value ['get_vpz_report'](./request_get_vpz_report.md) :
      for reports about a simulator (vpz) conditions and results
    - value ['post_vpz_report'](./request_post_vpz_report.md) :
      for reports about a modified simulator (vpz) conditions and results 
    - value ['get_vpz_report_conditions'](./request_get_vpz_report_conditions.md) :
      for reports about a simulator (vpz) conditions
    - value ['post_vpz_report_conditions](./request_post_vpz_report_conditions.md)' :
      for reports about a modified simulator (vpz) conditions

**Action selection** :

  - erecord_json tool case :

    The action is selected as 'action' key of the 'input_json' input 
    of erecord_json Galaxy tool.

    *The 'input_json' input of erecord_json Galaxy tool corresponds with
    the '-data' parameter of the main_json.py program.*

  - erecord_file tool case :

    The action is selected as the 'action_name' input
    of erecord_json Galaxy tool.

    *The 'action_name' input of erecord_json Galaxy tool corresponds with
    the '-action' parameter of the main_file.py program.*

**Limited** :

  - erecord_file tool accept all actions.
  - erecord_json tool does not accept actions
    that return 2 files (for that, use erecord_file tool), ie :
    'get_vpz_experiment', 'post_vpz_experiment',
    'get_vpz_report_conditions', 'post_vpz_report_conditions',
    'get_vpz_report', 'post_vpz_report'.

## [erecord_json](./erecord_json.md) tool

## [erecord_file](./erecord_file.md) tool

