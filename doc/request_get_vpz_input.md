# get_vpz_input request

## Description

Returns the input conditions of a simulator (vpz).

The input conditions corresponds with the configuration (experiment) defined into the vpz file (information such as parameters, duration, begin...).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'parselect' : [more](./requests.md)

- 'style' : [more](./requests.md)

  - Available values : 'tree', 'compact', 'compactlist'.
    Default value : 'compact'.

  - 'tree' value : for presentation by deploying information.
  - 'compact' value : for a compact presentation keeping only some useful
    parts of information.
  - 'compactlist' value : for a presentation even compacter than with
    'compact' style value, where in particular the vpz parameters are named
    by their 'selection_name'.

  Note : Values ordered from the fuller to the lighter style presentation :
  'tree', 'compact', 'compactlist'.

## Example

### Example A.

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "get_vpz_input", "pkgname": "wwdm", "vpzname": "wwdm.vpz",
    "style": "compactlist",
    "parselect": [
        "cond_meteo.meteo_file",
        "cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb", "cond_wwdm.Eimax",
        "cond_wwdm.K", "cond_wwdm.Lmax", "cond_wwdm.TI",
        "simulation_engine"
    ]
  }
  ```

- Response :

  ```
  {
    "begin": 0.0, 
    "duration": 222.0, 
    "cond_meteo.meteo_file": ["31035002.csv"], 
    "cond_wwdm.A": [0.0065], 
    "cond_wwdm.B": [0.00205], 
    "cond_wwdm.Eb": [1.85], 
    "cond_wwdm.Eimax": [0.94], 
    "cond_wwdm.K": [0.7], 
    "cond_wwdm.Lmax": [7.5], 
    "cond_wwdm.TI": [900.0], 
    "simulation_engine.begin": [0.0], 
    "simulation_engine.begin_date": ["2006-09-01"], 
    "simulation_engine.duration": [222.0], 
    "conds": ["cond_meteo", "cond_wwdm", "simulation_engine"], 
    "views": ["view"], 
    "outs": ["view.top:wwdm.LAI", "view.top:wwdm.ST", "view.top:wwdm.U"]
  }
  ```

### Example B.

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "get_vpz_input", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compact", 
    "parselect": ["cond_meteo.meteo_file", "cond_wwdm.A", "cond_wwdm.B"]
  }

  ```

- Response :

  ```
  {
    "begin": 0.0, 
    "duration": 222.0, 
    "pars": [
        {"cname": "cond_meteo", "pname": "meteo_file", "value": ["31035002.csv"], "selection_name": "cond_meteo.meteo_file"}, 
        {"cname": "cond_wwdm", "pname": "A", "value": [0.0065], "selection_name": "cond_wwdm.A"}, 
        {"cname": "cond_wwdm", "pname": "B", "value": [0.00205], "selection_name": "cond_wwdm.B"}
    ],
    "conds": [
        {"selection_name": "cond_meteo"}, 
        {"selection_name": "cond_wwdm"}
    ],
    "outs": [
        {"selection_name": "view.top:wwdm.LAI"}, 
        {"selection_name": "view.top:wwdm.ST"}, 
        {"selection_name": "view.top:wwdm.U"}
    ],
    "views": [{"selection_name": "view"}]
  }
  ```

### Example C.

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "get_vpz_input", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "tree", "parselect": "cond_meteo.meteo_file"
  }
  ```

- Response :

  ```
  {
    "vlebegin": {"value": 0.0}, 
    "vleduration": {"value": 222.0}, 
    "vlecond_list": [
        {
          "name": "cond_meteo", 
          "vlepar_list": [
              {"pname": "meteo_file", "cname": "cond_meteo", 
               "value": ["31035002.csv"], "type": "string", "selected": true}
          ]
        }
    ], 
    "vleview_list": [
        {
          "name": "view", "type": "timed", "timestep": 1.0, 
          "output_name": "view", "output_plugin": "storage",
          "output_format": "obsolete", "output_location": "",
          "vleout_list": [
              {"shortname": "LAI", "oname": "top:wwdm.LAI", "vname": "view", "selected": true}, 
              {"shortname": "ST", "oname": "top:wwdm.ST", "vname": "view", "selected": true}, 
              {"shortname": "U", "oname": "top:wwdm.U", "vname": "view", "selected": true}
          ]
        }
    ]
  }
  ```
