# post_vpz_inout request

In addition with the 'input_json' input containing this request,
there may be a 'datafolder' input (optional).

## Description

Maybe modifies the input conditions of a simulator (vpz)
(depending on the request options : see 'pars', 'begin'...).

In addition with this input conditions modification (optional), 
maybe modifies the input datas folder of the simulator,
according to the 'datafolder' input (optional).

Then runs the simulation.

And finally returns the simulation results and also the input conditions.

## Help

Before a 'post_vpz_inout' request, it may be useful to do a 'get_vpz_input'
request with 'compact' or 'compactlist' values for 'style'
(see [more](./requests.md)), in order to make easier some modifications
(see 'pars', 'parselect', 'outselect' options).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)
- 'begin' : [more](./requests.md)
- 'duration' : [more](./requests.md)
- 'pars' or cname.pname : [more](./requests.md)
- 'datafoldercopy' : [more](./requests.md).
  Option in relation with the maybe joined 'datafolder' input.
- 'parselect' : [more](./requests.md)
- 'outselect' : [more](./requests.md)
- 'style' : [more](./requests.md)
  - Available values : only 'tree' ('compact', 'compactlist' : unavailable).

## Example

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_inout", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "single", "restype": "dataframe", 
    "parselect": ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.K"], 
    "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
    "cond_wwdm.B": 0.00201, 
    "duration": 6.0, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```

- Response :

  ```
  {
    "vpzname": "wwdm.vpz", "pkgname": "wwdm", "vleversion": "vle-2.0.0", 
    "vpzinput": {"begin": 0.0, "duration": 6.0, "cond_wwdm.A": [0.006300000008196, 0.00650000013411, 0.006699999794364], "cond_wwdm.B": [0.002009999938309], "cond_wwdm.K": [0.7], "conds": ["cond_wwdm"], "views": ["view"], "outs": ["view.top:wwdm.LAI"]
    }, 
    "vpzoutput": {
        "res": {
            "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
            "view.top:wwdm.LAI": [0.0, 0.002545988090392853, 0.005772484250258919, 0.009972627790810543, 0.015516120447965248, 0.0220378266162036, 0.029709283776693275]
        },
        "plan": "single", "restype": "dataframe"
    }
  }

  ```
