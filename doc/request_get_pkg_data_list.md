# get_pkg_data_list request

## Description

Returns, for a Vle package (pkg), the list of its data file names.
Data files are those present into its default 'data' folder.

## Request options

- 'pkgname' (**required**) : [more](./requests.md)

## Example

- Request (erecord_json tool case => 'action' included) :

  ```
  {"action":"get_pkg_data_list", "pkgname":"wwdm"}
  ```

- Response :

  ```
  {"datalist": ["31035002.csv", "testsWithoutErrors.txt"], "pkgname": "wwdm"}
  ```

