# get_vpz_report_conditions request

## Description

Produces and returns a .xls file (as 'conditions' output)
from the input conditions of a simulator (vpz).


## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'mode' for 'bycol/byrow' : [more](./requests.md)

## Example

TODO

