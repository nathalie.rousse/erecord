# erecord_json tool

- Inputs:

  - input_json (optional) : data json file containing the request options.

    *More exactly in case of main_json.py program, this input corresponds with
    the '-data' parameter (data json file path).*

    - The 'action' value is contained into input_json :

      - Available values :
        - 'help',
        - ['get_vpz_input'](./request_get_vpz_input.md),
        - ['post_vpz_input'](./request_post_vpz_input.md),
        - ['get_vpz_output'](./request_get_vpz_output.md),
        - ['post_vpz_output'](./request_post_vpz_output.md),
        - ['get_vpz_inout'](./request_get_vpz_inout.md),
        - ['post_vpz_inout'](./request_post_vpz_inout.md),
        - ['get_pkg_list'](./request_get_pkg_list.md),
        - ['get_pkg_vpz_list'](./request_get_pkg_vpz_list.md),
        - ['get_pkg_data_list'](./request_get_pkg_data_list.md).
    
      - Unavailable values :
        'get_vpz_experiment', 'post_vpz_experiment',
        'get_vpz_report_conditions', 'post_vpz_report_conditions',
        'get_vpz_report', 'post_vpz_report'.

  - datafolder (optional) : zip file containing the new input datas folder
    to be taken into account.

    *More exactly in case of main_json.py program, this input corresponds with
    the '-datafolder' parameter (datafolder file path).*

    - Available and used only if 'action' values :
      'post_vpz_output', 'post_vpz_inout'.
    - The new input datas folder must be named 'data' (data/...).
    - The original input datas folder of the simulator will be replaced or
      overwritten by the new one, depending on 'datafoldercopy' option value
      defined into 'input_json' input (see [more](./requests.md)).
    - Note: Anything else than the folder named 'data', that may be contained
      into the .zip file will be ignored (files, folders...).
    - Note: The user must ensure that datas files delivered into
      the new input datas folder corresponds with what the simulator expects
      (format, values...).
    - Examples :
      in ['post_vpz_output'](./request_post_vpz_output.md) request examples.

- Outputs (one .json file):

  - output_json (always) : data json file. Contains response and request.

