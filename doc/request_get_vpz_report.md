# get_vpz_report request

## Description

Produces required reports, from the simulation results of a simulator (vpz)
after having run simulation, and also from its input conditions.

Returns a .zip file (as 'report' output) gathering the built reports.

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)
- 'report' : [more](./requests.md)
- 'mode' for 'bycol/byrow' : [more](./requests.md)

## Example

TODO

