# post_vpz_input request

## Description

Returns the input conditions of a simulator (vpz), maybe after modifications
(depending on the request options : see 'pars', 'begin'...).

The input conditions corresponds with the configuration (experiment) defined into the vpz file (information such as parameters, duration, begin...).

## Help

Before a 'post_vpz_input' request, it may be useful to do a 'get_vpz_input'
request with 'compact' or 'compactlist' values for 'style'
(see [more](./requests.md)), in order to make easier some modifications
(see 'pars', 'parselect' options).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'begin' : [more](./requests.md)
- 'duration' : [more](./requests.md)
- 'pars' or cname.pname : [more](./requests.md)
- 'parselect' : [more](./requests.md)
- 'outselect' : [more](./requests.md)

- 'style' : [more](./requests.md)

  - Available values : 'tree', 'compact', 'compactlist'.
    Default value : 'compact'.

  - 'tree' value : for presentation by deploying information.
  - 'compact' value : for a compact presentation keeping only some useful
    parts of information.
  - 'compactlist' value : for a presentation even compacter than with
    'compact' style value, where in particular the vpz parameters are named
    by their 'selection_name'.

  Note : Values ordered from the fuller to the lighter style presentation :
  'tree', 'compact', 'compactlist'.

## Example

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_input", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", 
    "parselect": ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.K"], 
    "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
    "cond_wwdm.B": 0.00201, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```
- Response :

  ```
  {
    "begin": 0.0, 
    "duration": 222.0, 
    "cond_wwdm.A": [0.006300000008196, 0.00650000013411, 0.006699999794364], 
    "cond_wwdm.B": [0.002009999938309], 
    "cond_wwdm.K": [0.7], 
    "conds": ["cond_wwdm"], 
    "views": ["view"], 
    "outs": ["view.top:wwdm.LAI"]
  }
  ```

