# Request

The request information defines what is wanted,
it is a **json data** containing some options.
The validity of an option depends on the selected action.

Examples (erecord_json tool case => 'action' included) :

  ```
  {"action":"help"}
  {"action":"get_pkg_list"}
  {"action":"get_pkg_vpz_list", "pkgname":"erecord"}
  {"action":"get_vpz_input", "pkgname":"wwdm", "vpzname":"wwdm.vpz"}
  {"action":"post_vpz_output", "pkgname":"wwdm", "vpzname":"wwdm.vpz", "duration":20.0}
  ```
The existing **request options** are listed below.

## pkgname

The 'pkgname' option is used to choose a Vle package (pkg).

- 'pkgname' value : the name of an existing Vle package.

Note : ['get_pkg_list'](./request_get_pkg_list.md) request to know existing Vle packages.

## vpzname

The 'vpzname' option associated with the 'pkgname' option is used
to choose a simulator (vpz).

A simulator (vpz) corresponds with the Vle notion of vpz, or vpz file.

- 'vpzname' value : the simulator vpz file name.
- 'pkgname' value : the name of the Vle package containing the simulator
  (vpz file).

Note : ['get_pkg_vpz_list'](./request_get_pkg_vpz_list.md) request to know existing simulators.

## begin
    
- 'begin' value : the new experiment 'begin' value.

## mode for bycol/byrow

The 'mode' option is used to choose to have some
arrays presented by colums instead of, by default, by rows.

- 'mode' value 'bycol' : for some arrays to be by columns.
- 'mode' value 'byrow' : for some arrays to be by rows (default).

This option is available for 'action' values :
get_vpz_report, post_vpz_report,
get_vpz_report_conditions, post_vpz_report_conditions.

## datafoldercopy

The 'datafoldercopy' option is used to choose the way how to take into account the new input datas folder sent by the user ('datafolder' input).

- 'datafoldercopy' value 'replace' :
  the new input datas folder replaces the original one.

- 'datafoldercopy' value 'overwrite' :
  the content of the new input datas folder is added to the original one
  (by overwriting).

## duration

- 'duration' value : the new experiment 'duration' value.

## mode

The 'mode' option is used for different choices that are available or not 
according to the 'action' case. Some may be incompatible each other.

Type of 'mode' option : a string or a list of strings.

- Style : see 'style' option.

- Simulation running mode : see 'plan' and 'restype' options.

## outselect

The 'outselect' option is used to choose the restituted output datas.

- 'outselect' value 'all' : to select all output datas of all views.

- 'outselect' value viewname : to select all output datas
  of the view named viewname.

- 'outselect' value vname.oname : to select the ouput data named oname
  of the view named vname (the output datas of this view, that are not
  selected like this, are unselected).

## parselect

The 'parselect' option is used to choose the restituted parameters.

- 'parselect' value 'all' : to select all parameters of all conditions.

- 'parselect' value condname : to select all parameters of the condition
  named condname.

- 'parselect' value cname.pname : to select the parameter named pname
  of the condition named cname (the parameters of this condition,
  that are not selected like this, are unselected).

## pars or cname.pname

The 'pars' option is used to modify values of some parameters of the
experiment :

  - 'pars' is a list of dict, each dict being dedicated to a parameter.

    The dict of the parameter named pname of the condition named cname 
    with s as selection_name (s valuing cname.pname) is :
    { "selection_name": s, "cname": cname, "pname": pname, "value": newvalue }

There is another way to modify the value of this parameter, by using
the option cname.pname whose name is composed of cname and pname :

  - cname.pname=newvalue

## plan

The 'plan' option is used to choose the simulation running mode 
(type of plan).

- 'plan' values : 'single', 'linear' (default value : 'single').

Note : The 'mode' option can be used instead of 'plan' option :

  - mode=single and plan=single are equivalent
  - mode=linear and plan=linear are equivalent

## restype

The 'restype' option is used to choose the simulation running mode 
(type of results).

- 'restype' values :  'dataframe', 'matrix' (default value : 'dataframe').

Note : The 'mode' option can be used instead of 'restype' option :

  - mode=dataframe and restype=dataframe are equivalent
  - mode=matrix    and restype=matrix    are equivalent

## report

The 'report' option is used to choose which reports are built into the
returned .zip report file (as 'report' output).

Type of 'report' option : a string or a list of strings.

- 'report' value 'vpz' : for the vpz file into the report folder.
- 'report' value 'csv' : for csv file(s) into the report folder.
- 'report' value 'xls' : for a xls file into the report folder
  (several worksheets).
- 'report' value 'txt' : for txt file(s) into the report folder.
- 'report' value 'all' : for all the available files above (txt, csv...)
  into the report folder.

'report' option is available for 'action' values :
get_vpz_report, post_vpz_report.

## style

The 'style' option is used to select the style presentation.

- 'style' value 'tree' for presentation by deploying information.
  'tree' is the main 'style' value.
- 'style' values 'compact' and 'compactlist' : 2 other 'style' values,
  that are not always available (depending on the request).

Note : The 'mode' option can be used instead of 'style' option :

  - mode=tree        and style=tree        are equivalent
  - mode=compact     and style=compact     are equivalent
  - mode=compactlist and style=compactlist are equivalent

Note : 'compact' and 'compactlist' values for 'style' 

  - can be helpful to prepare a following request
    (where parameters modification, output datas selection...),
  - can be handy to then exploit the returned datas
    (output datas, parameters...). 

