# post_vpz_experiment request

In addition with the 'input_json' input containing this request :
  - there is a .xls file as 'experimentfile' input (**mandatory**).
  - there may be a 'datafolder' input (optional).

## Description

Modifies the experiment conditions of a simulator (vpz)
according to the .xls file joined as 'experimentfile' input (mandatory).

In addition with this experiment conditions modification,
maybe modifies the input datas folder of the simulator,
according to the 'datafolder' input (optional).

Then runs the simulation.

Produces a .xls file containing
the simulation results + the experiment conditions.

And finally returns this produced .xls file as 'experiment' output.

## 'experimentfile' input

The .xls file joined (as 'experimentfile' input)
with such a 'post_vpz_experiment' request has the same format and structure
than the .xls file returned (as 'experiment' output)
by a 'get_vpz_experiment' request.

The joined .xls file ('experimentfile' input) contains
the experiment conditions from which the simulator has to be updated :

  - General information :
    - plan *('single' or 'linear' value)*,
    - restype *('dataframe' or 'matrix' value)*,
    - begin, duration.
  - Parameters values : list of the parameters values to be modified
    *(the unchanged values may remain but it is not necessary)*.
  - Parameters identification : list of the parameters to be given into
    the returned .xls file ('experiment' output).
  - Output datas identification : list of the output datas to be given into
    the returned .xls file ('experiment' output).

## Help

Before such a 'post_vpz_experiment' request, it may be useful to do a
'get_vpz_experiment' request, in order to easierly build 
the .xls file ('experimentfile' input) that is required
with 'post_vpz_experiment' request. Indeed :

  - The .xls file joined with a 'post_vpz_experiment' request
    has the same format and structure than
    the .xls file returned by the 'get_vpz_experiment' request.
  - The .xls file returned by the 'get_vpz_experiment' request
    contains some instructions (in blue text) helping to modify it,
    if wanted to use it as the .xls file
    joined with a 'post_vpz_experiment' request.

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'datafoldercopy' : [more](./requests.md).
  Option in relation with the maybe joined 'datafolder' input.

Note : The experiment conditions ('plan', 'restype', 'duration', 'pars'...) are **not** defined here into the request ('input_json' input) : they are defined into the joined 'experimentfile' input (**mandatory**).

## Response

The returned .xls file ('experiment' output) contains the simulation results
and experiment conditions of the simulator modified according to the 
.xls file joined as 'experimentfile' input :

  - Simulation results :
    - Values array(s) of the selected output datas.
  - Experiment conditions :
    - General information : plan, restype, begin, duration according to
      'experimentfile' input.
    - Parameters values : list of the selected parameters values according
      to 'experimentfile' input.
    - Parameters identification : list of the selected parameters.
    - Output datas identification : list of the selected output datas.

Only the selected output datas and parameters are present into the returned .xls file ('experiment' output) :

  - a parameter is selected if it has been declared
    into the 'experimentfile' input,
    in the dedicated list "**Parameters identification**".
  - an output data is selected if it has been declared
    into the 'experimentfile' input,
    in the dedicated list "**Output datas identification**".

**Limit** : in case of xls file overflow
*(sheets limited to 256 columns and 65536 rows)* the returned .xls file
('experiment' output) will be truncated (without any warning).

## Example

**erecord_file tool case**

- Inputs :

  - action_name = 'post_vpz_experiment'

  - input_json (containing the request) :

    ```
    {"pkgname":"wwdm", "vpzname":"wwdm.vpz"}
    ```

  - experimentfile :  [experimentfile.xls](./inputs/Galaxy3-experiment_modif.xls) file (containing the experiment plan).

    Note : this file has been built from
    [experiment.xls](./outputs/Galaxy3-[experiment].xls) file that has
    previously been got by
    ['get_vpz_experiment'](./request_get_vpz_experiment.md) request,
    and modified according to the desired experiment plan.

- Outputs :

  - output_json

  - experiment : [experiment.xls](./outputs/Galaxy14-[experiment].xls) file
    (containing the simulation results and the experiment plan).

