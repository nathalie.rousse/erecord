# post_vpz_output request

In addition with the 'input_json' input containing this request,
there may be a 'datafolder' input (optional).

## Description

Maybe modifies the input conditions of a simulator (vpz)
(depending on the request options : see 'pars', 'begin'...).

In addition with this input conditions modification (optional), 
maybe modifies the input datas folder of the simulator,
according to the 'datafolder' input (optional).

Then runs the simulation and returns the simulation results.

## Help

Before a 'post_vpz_output' request, it may be useful to do a 'get_vpz_input'
request with 'compact' or 'compactlist' values for 'style'
(see [more](./requests.md)), in order to make easier some modifications
(see 'pars', 'outselect' options).

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)
- 'begin' : [more](./requests.md)
- 'duration' : [more](./requests.md)
- 'pars' or cname.pname : [more](./requests.md)
- 'datafoldercopy' : [more](./requests.md).
  Option is in relation with the maybe joined 'datafolder' input.
- 'outselect' : [more](./requests.md)

- 'style' : [more](./requests.md)

  - Available values : 'tree', 'compact', and also 'compactlist'.
    Default value : 'tree'.
  
  - 'tree' value : for presentation by deploying information.
  - 'compact' value : for the output datas to be named by
    their 'selection_name'.
  - 'compactlist' value : has here same meaning as 'compact'.

  - 'compact' (and 'compactlist') 'style' value
    has an effect only if 'restype' values 'dataframe'
    (has no effect if 'restype' values 'matrix').

## Examples

  - Example A. :
    "style": "compactlist" | "plan": "single" | "restype": "dataframe"
  - Example B. : 
    "style": "compactlist" | "plan": "linear" | "restype": "dataframe"
  - Example C. :
    "style": "compactlist" | "plan": "linear" | "restype": "matrix"
  - Example D. :
    With a 'datafolder' input (.zip file) | "datafoldercopy": "replace"
  - Example E. :
    With a 'datafolder' input (.zip file) | "datafoldercopy": "overwrite"

### Example A.

"style": "compactlist" | "plan": "single" | "restype": "dataframe"

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "single", "restype": "dataframe", 
    "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, 
    "duration": 6.0, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```

- Response :

  ```
  {
    "res": {
        "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0],
        "view.top:wwdm.LAI": [0.0, 0.002545988090392853, 0.005772484250258919, 0.009972627790810543, 0.015516120447965248, 0.0220378266162036, 0.029709283776693275]
    }, 
    "plan": "single", "restype": "dataframe"
  }
  ```

### Example B.

"style": "compactlist" | "plan": "linear" | "restype": "dataframe"

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "linear", "restype": "dataframe", 
    "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, 
    "duration": 6.0, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```

- Response :

  ```
  {
    "res": [
      {
        "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
        "view.top:wwdm.LAI": [0.0, 0.002545988090392853, 0.005772484250258919, 0.009972627790810543, 0.015516120447965248, 0.0220378266162036, 0.029709283776693275]
      }, 
      {
        "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
        "view.top:wwdm.LAI": [0.0, 0.0022339759017648317, 0.005077554633386796, 0.00879635724251485, 0.013728811240568645, 0.019560680558557447, 0.02645400034770933]
      }, 
      {
        "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
        "view.top:wwdm.LAI": [0.0, 0.001955794243304024, 0.0044562042510456375, 0.007741259803822401, 0.01211982091608149, 0.017322552300504723, 0.023501935008506628]
      }
    ], 
    "plan": "linear", "restype": "dataframe"
  }
  ```

### Example C.

"style": "compactlist" | "plan": "linear" | "restype": "matrix"

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "linear", "restype": "matrix", 
    "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, 
    "duration": 6.0, "outselect": "view"
  }
  ```

- Response :

  ```
  {
    "res": [
      {
        "view": [
          [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
          [0.0, 0.002545988090392853, 0.005772484250258919, 0.009972627790810543, 0.015516120447965248, 0.0220378266162036, 0.029709283776693275], 
          [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002], 
          [0.0, 0.02701671148387024, 0.10341972233877447, 0.23419267750991066, 0.44947532155698766, 0.7299253012577255, 1.0237747532539703]
        ]
      }, 
      {"view": [
          [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
          [0.0, 0.0022339759017648317, 0.005077554633386796, 0.00879635724251485, 0.013728811240568645, 0.019560680558557447, 0.02645400034770933], 
          [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002], 
          [0.0, 0.02370838737949498, 0.09092983895045417, 0.20632558190015393, 0.3969287097795553, 0.6460702998551973, 0.9080196045100235]
        ]
      }, 
      {"view": [
          [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
          [0.0, 0.001955794243304024, 0.0044562042510456375, 0.007741259803822401, 0.01211982091608149, 0.017322552300504723, 0.023501935008506628], 
          [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002], 
          [0.0, 0.020758165412541284, 0.0797664194260004, 0.18135825215352658, 0.34971770079760617, 0.5705251001853786, 0.8034827258970842]
        ]
      }
    ], 
    "plan": "linear", "restype": "matrix"
  }
  ```

### Example D.

With a 'datafolder' input (.zip file) : [data.zip](./inputs/data.zip)
containing the new input datas folder to be taken into account,
according to 'datafoldercopy' value.

"datafoldercopy" : "replace"

[data.zip](./inputs/data.zip) content : 

    data/31035002_bis.csv

This 'post_vpz_output' request
uses '31035002_bis.csv' file (from [data.zip](./inputs/data.zip)) and
does not use '31035002.csv' (from original data folder of "wwdm" simulator)
=> 'datafoldercopy' can value 'replace'.

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "single", "restype": "dataframe", 
    "cond_meteo.meteo_file": ["31035002_bis.csv"], 
    "datafoldercopy": "replace", 
    "duration": 6.0, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```

- Response :

  ```
  {
    "res": {
        "view.time": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
        "view.top:wwdm.LAI": [0.0, 0.002214909359123069, 0.005036364374387277, 0.008728880157686643, 0.013629812139229687, 0.01942796680153359, 0.026285059007071357]
    }, 
    "plan": "single", "restype": "dataframe"
  }
  ```

### Example E.

With a 'datafolder' input (.zip file) : [data.zip](./inputs/data.zip)
containing the new input datas folder to be taken into account,
according to 'datafoldercopy' value.

"datafoldercopy" : "overwrite"

[data.zip](./inputs/data.zip) content : 

    data/31035002_bis.csv

This 'post_vpz_output' request 
uses '31035002_bis.csv' file (from [data.zip](./inputs/data.zip))
and still uses '31035002.csv' (from original data folder of "wwdm" simulator)
=> 'datafoldercopy' must value 'overwrite'.

- Request (erecord_json tool case => 'action' included) :

  ```
  {
    "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", 
    "style": "compactlist", "plan": "linear", "restype": "matrix", 
    "cond_meteo.meteo_file": ["31035002.csv", "31035002_bis.csv"], 
    "datafoldercopy": "overwrite", 
    "duration": 6.0, 
    "outselect": "view.top:wwdm.LAI"
  }
  ```

- Response :

  ```
  {
    "res": [
        {"view": [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0],
                  [0.0, 0.002214909359123069, 0.005036364374387277, 0.008728880157686643, 0.013629812139229687, 0.01942796680153359, 0.026285059007071357]]
        }, 
        {"view": [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0], 
                  [0.0, 0.002214909359123069, 0.005036364374387277, 0.008728880157686643, 0.013629812139229687, 0.01942796680153359, 0.026285059007071357]]
        }
    ], 
    "plan": "linear", "restype": "matrix"
  }

  ```
