# get_pkg_list request

## Description

Returns the list of existing Vle packages (pkg) names.

## Request options

- 'style' : [more](./requests.md)
  - Available values : only 'tree' ('compact', 'compactlist' : unavailable).

## Example

- Request (erecord_json tool case => 'action' included) :

  ```
  {"action":"get_pkg_list"}
  ```

- Response :

  ```
  {"pkglist": ["DateTime", "erecord", "record.meteo", "vle.discrete-time", "vle.reader", "vle.tester", "wwdm"]}
  ```

