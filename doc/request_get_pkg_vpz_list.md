# get_pkg_vpz_list request

## Description

Returns, for a Vle package (pkg), the list of its simulators (vpz) names.

## Request options

- 'pkgname' (**required**) : [more](./requests.md)
- 'style' : [more](./requests.md)
  - Available values : only 'tree' ('compact', 'compactlist' : unavailable).

## Example

- Request (erecord_json tool case => 'action' included) :

  ```
  {"action": "get_pkg_vpz_list", "pkgname": "wwdm"}
  ```

- Response :

  ```
  {"vpzlist": ["wwdm.vpz"], "pkgname": "wwdm"}
  ```

