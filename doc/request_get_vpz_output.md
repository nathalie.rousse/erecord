# get_vpz_output request

## Description

Returns the simulation results of a simulator (vpz)
after having run simulation.

## Request options

- 'pkgname' and 'vpzname' (**required**) : [more](./requests.md)
- 'plan' and 'restype' : [more](./requests.md)

- 'style' : [more](./requests.md)

  - Available values : 'tree', 'compact', and also 'compactlist'.
    Default value : 'tree'.

  - 'tree' value : for presentation by deploying information.
  - 'compact' value : for the output datas to be named by
    their 'selection_name'.
  - 'compactlist' value : has here same meaning as 'compact'.

  - 'compact' (and 'compactlist') 'style' value
    has an effect only if 'restype' values 'dataframe'
    (has no effect if 'restype' values 'matrix').

## Example

TODO

