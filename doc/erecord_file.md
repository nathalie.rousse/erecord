# erecord_file tool

- Inputs:

  - action_name : 'action' value.

    *More exactly in case of the main_file.py program, this input corresponds
    with the '-action' parameter (action value).*

    - Available values :

      - cases where only 1 file returned :
        - 'help',
        - ['get_vpz_input'](./request_get_vpz_input.md),
        - ['post_vpz_input'](./request_post_vpz_input.md),
        - ['get_vpz_output'](./request_get_vpz_output.md),
        - ['post_vpz_output'](./request_post_vpz_output.md),
        - ['get_vpz_inout'](./request_get_vpz_inout.md),
        - ['post_vpz_inout'](./request_post_vpz_inout.md),
        - ['get_pkg_list'](./request_get_pkg_list.md),
        - ['get_pkg_vpz_list'](./request_get_pkg_vpz_list.md),
        - ['get_pkg_data_list'](./request_get_pkg_data_list.md).

      - cases where 2 files returned :
        - ['get_vpz_experiment'](./request_get_vpz_experiment.md),
        - ['post_vpz_experiment'](./request_post_vpz_experiment.md),
        - ['get_vpz_report_conditions'](./request_get_vpz_report_conditions.md),
        - ['post_vpz_report_conditions](./request_post_vpz_report_conditions.md)',
        - ['get_vpz_report'](./request_get_vpz_report.md),
        - ['post_vpz_report'](./request_post_vpz_report.md).

  - input_json (optional) : data json file containing the request options
    EXCLUDING 'action' (an 'action' value into input_json would be ignored).

    *More exactly in case of the main_file.py program, this input corresponds
    with the '-data' parameter (data json file path).*

  - datafolder (optional) : zip file containing the new input datas folder
    to be taken into account.

    *More exactly in case of the main_file.py program, this input corresponds
    with the '-datafolder' parameter (datafolder file path).*

    - Available and used only if 'action' values :
      'post_vpz_output', 'post_vpz_inout',
      'post_vpz_experiment', 'post_vpz_report'.
    - The new input datas folder must be named 'data' (data/...).
    - The original input datas folder of the simulator will be replaced or
      overwritten by the new one, depending on 'datafoldercopy' option value
      defined into 'input_json' input (see [more](./requests.md)).
    - Note: Anything else than the folder named 'data', that may be contained
      into the .zip file will be ignored (files, folders...).
    - Note: The user must ensure that datas files delivered into
      the new input datas folder corresponds with what the simulator expects
      (format, values...).
    - Examples : in
      ['post_vpz_output'](./request_post_vpz_output.md) request examples.

  - experimentfile : experiment.xls file containing the experiment plan.

    *More exactly in case of the main_file.py program, this input corresponds
    with the '-experimentfile' parameter (experimentfile path).*

    - Available and required only if 'action' values 'post_vpz_experiment'.
    - To build experiment.xls, you can get it by a 'get_vpz_experiment' 'action'
      then modify it according to your experiment plan (respecting format !).
    - For more, see ['post_vpz_experiment'](./request_post_vpz_experiment.md)
      request.
    - Examples : in
      ['post_vpz_experiment'](./request_post_vpz_experiment.md) request
      examples.

- Outputs (one .json file + maybe one more file .zip or .xls):

  - output_json (always) : data json file. Contains response and request.

  - experiment : experiment.xls file.
    - Only if 'action' values 'get_vpz_experiment', 'post_vpz_experiment'.
    - For more, see ['get_vpz_experiment'](./request_get_vpz_experiment.md),
      ['post_vpz_experiment'](./request_post_vpz_experiment.md) requests.
    - Examples : in ['get_vpz_experiment'](./request_get_vpz_experiment.md)
      and ['post_vpz_experiment'](./request_post_vpz_experiment.md) requests
      examples.

  - conditions : conditions.xls file.
    - Only if 'action' values
      'get_vpz_report_conditions', 'post_vpz_report_conditions'.

  - report : report.zip file.
    - Only if 'action' values 'get_vpz_report', 'post_vpz_report'.
    - For more, see ['get_vpz_report'](./request_get_vpz_report.md),
      ['post_vpz_report'](./request_post_vpz_report.md) requests.
    - Examples : in ['post_vpz_report'](./request_post_vpz_report.md) request
      examples.

