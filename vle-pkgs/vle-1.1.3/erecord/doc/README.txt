###############################################################################
#
#                            erecord package
#
###############################################################################

###############################################################################
# Simulator : read_vpz.vpz                               Dynamic : ReadVpz.cpp
#

- Input data file  : read_input.json
- Output data file : read_output.json

# Description :
Reads the vpz file as defined by read_input.json input file and writes its read
input information into the read_output.json output file.

# read_input.json content :
  - pkgname
  - vpzname

# read_output.json content :

  - experiment name
  - begin value
  - duration value

  - "cond_list" : for each condition, all of its parameters (value, type)
    { 'condition name' : { 'parameter name' : {'value':value,'type':type} } }

  - "view_list" : for each view,
    - type
    - timestep
    - output information : name, plugin, format, location
    - all of its output datas (name not prefixed by view name)

  - "names" :
    - conditions names
    - parameters names
    - views names
    - output datas names

###############################################################################
# Simulator : modify_vpz.vpz                           Dynamic : ModifyVpz.cpp
#

- Input data file : modify_input.json
- Result          : modified vpz file

# Description :
Modifies the vpz file as defined by modify_input.json input file and saves it.

# modify_input.json content :
  - pkgname
  - vpzname
  - begin
  - duration

  - "conditions" : values of their parameters to be modified

  - "views" : 
    - "storaged_list" : information of views and/or output datas to be
      activated ie set in "storage" plugin.
      Available values : "all", some vname, some vname.oname.

  Note about "conditions" :
  - The information about parameters (given into "conditions") is given as
    cname[pname] or cname.pname.

  Notes about "views" :
  - If "storaged_list" is absent then no plugin of any view is modified.
  - A view is activated (respectively desactivated) by setting its output
    plugin in "storage" (respectively "dummy") value.
  - The activation/desactivation is also applied at level of each
    output data (by remaining attached/detaching views to observable ports).
  - An output data remains activated (respectively is desactivated) by
    remaining attached (respectively detaching) the relevant view from its
    observation.

# An illustration of "modify_input.json" content :

     {
       "begin":value,
       "duration":value,
  
       "conditions" :
          {
          "cnameA" : { "pname_a":values1, "pname_b":values2, ...},
          "cnameB" : { "pnames_c":values3, "pname_d":values4, ...},
          ...,
          "cnameC.pname_e":values5,
          "cnameC.pname_f":values6,
          "cnameD.pname_g":values7,
          ...,
          },
  
       "views" :
          {
          "storaged_list" :
             [
             "all",
             "vnameA", "vnameB", ...,
             "vnameC.oname_a", "vnameC.oname_b", "vnameD.oname_c", ...,
             ...,
             ],
          },
       ...,
     }
     
###############################################################################
# Simulator : run_vpz.vpz                                 Dynamic : RunVpz.cpp

- Input data file : run_input.json
- Output data file : run_output.json

# Description :
Runs the simulation of the vpz file as defined by run_input.json input file and
writes the simulation results into the run_output.json output file.

The simulation results that are recorded into run_output.json are those of
views in 'storage' plugin. There may exist some other simulation results, as
files, in case of views in 'file' plugin.

# run_input.json content :
  - pkgname
  - vpzname
  - plan (values : "linear" "single")
  - restype (values : "matrix" "dataframe")

# run_output.json content :
  - restype
  - "res" : simulation results

###############################################################################
# Examples

Produced by :
vle -P erecord read_vpz.vpz
mv read_output.json read_output.initial.json
vle -P erecord modify_vpz.vpz
vle -P erecord read_vpz.vpz
vle -P erecord run_vpz.vpz

###############################################################################

