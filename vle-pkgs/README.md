# vle-pkgs

The erecord/vle-pkgs folder contains the 'erecord' vle package
for different vle versions.

The use of the 'erecord' python App for a vle model requires that
the 'erecord' vle package has been installed with the model vle packages
into the vle 'pkgs' folder.

