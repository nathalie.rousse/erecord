import setuptools

setuptools.setup(
    name="erecord",
    version="0.0.1",
    author="Nathalie Rousse",
    author_email="nathalie.rousse@inrae.fr",
    description="erecord python library",
    long_description_content_type="text/markdown",
    url="https://forgemia.inra.fr/nathalie.rousse/erecord/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8"
    ],
    install_requires=["Django", "djangorestframework",
                      "djangorestframework-xml", "djangorestframework-jsonp",
                      "djangorestframework-yaml", "djangorestframework-jwt",
                      "xlwt", "xlrd"],
)
