# erecord

## Desc

Project gathering erecord source code :

  - 'erecord' folder : erecord python library
  - 'vle-pkgs' folder : erecord vle package(s)

The 'erecord' python library can be used for some vle models that have
previously been installed into the vle 'pkgs' folder. This 'pkgs' folder
must also contain the 'erecord' vle package (from 'vle-pkgs').

## Deployment 

See project erecord-deploy :

- erecord-deploy/containers folder : containers for some vle models that will be
  able to be used by erecord python library, directly as containers
  (```singularity exec ...```) or by Galaxy tools based on those containers.

  Those containers contain : vle, the models and 'erecord' vle packages,
  python and the erecord python library.

- erecord-deploy/galaxy-tools folder : some Galaxy tools based on containers
  from erecord-deploy/containers.

## 'erecord' python library

- [Documentation](./doc/README.md)

- Install :

  - source :
    How to create and publish a python package on Gitlab PyPi Repository ?
    https://medium.com/maisonsdumonde/gitlab-ci-cd-and-gitlab-pypi-repository-4916a51d22eb

  - create python virtual environment :
  ```
      python3 -m venv MYVENV
      source MYVENV/bin/activate
      python3 -m pip install --upgrade pip wheel setuptools
  ```

  - Install 'erecord' package :
  ```
      python3 setup.py bdist_wheel
      # => dist/erecord-0.0.1-py3-none-any.whl created
      python3 -m pip install dist/erecord-0.0.1-py3-none-any.whl
  ```

- Install 'erecord' python library into containers :
  see vle_*.def into erecord-deploy/containers.

